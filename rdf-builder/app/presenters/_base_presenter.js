/**
 * Created by radims on 29.01.16.
 */
var template_system = require("../../modules/template-system/template-system");
var basicFileMapper = require("../sukl/model/basic-file-mapper");
var path = require("path");
//number which slicing point in array
const ARRAY_SLICE_NUMBER = 40000;
/**
 * This presenter render template. Data, which will be rendered, was downloaded from sukl.cz. This data
 * is in encoding cp1250. Function convert encoding from cp1250 to utf8, read file and create object. This object we get
 * to template and render.
 * @param input_path - input path of file
 * @param file_name - file name of file
 * @param template_path - path, where the template is
 * @param template_output - path, where function save the result
 * @param mapper - abstract object which creates specific object type
 * @param template_vars - some extra vars, we want to add to template
 * @param callback - callback function
 */
module.exports.render = function (input_path, file_name,template_path,template_output, mapper, template_vars, callback) {
    basicFileMapper.mapAll(input_path, file_name,  mapper,
        function(array) {
            var counter = 0;

            var fileName = template_output
            var dirname =  path.dirname(fileName);
            var fileExt = path.extname(fileName);
            var fileBaseName = path.basename(fileName,path.extname(fileName));
            //parsing only rdfs to some different files
            if(fileExt == ".rdf") {
                for (var i = 0; i < Math.ceil(array.length / ARRAY_SLICE_NUMBER); i++) {
                    var start_index = (i * ARRAY_SLICE_NUMBER + Boolean(i));
                    var end_index = ((i * ARRAY_SLICE_NUMBER) + ARRAY_SLICE_NUMBER);
                    var newFileName = fileBaseName + (i != 0? "("+i+")": "") + fileExt;

                    var template_system_object = {
                        mapper_output: array.slice(start_index,end_index),
                        template: template_vars
                    };
                    template_system.render(
                        template_path,
                        dirname +"/"+  newFileName,
                        template_system_object

                    );

                }
            }else{
                var template_system_object = {
                    mapper_output: array.slice(start_index,end_index),
                    template: template_vars
                };

                template_system.render(
                    template_path,
                    template_output,
                    template_system_object

                );
            };

            callback


        }
    );
}
