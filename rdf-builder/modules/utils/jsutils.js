/**
 * Created by radims on 05.02.16.
 */


/**
 * If some variable null or undefined return false
 * @param array Array
 */
function isSomeEmpty(array) {

    if(!isArray(array))
        throw new Error("Variable is not array");
    var filter = array.filter(function(val) {
        return isNullOrUndefined(val) ? false : true;
    });
    return filter.length === array.length ? false : true;
};

function isNullOrUndefined(value) {
    return value === undefined || value === null ? true:false;
}

function isArray(variable) {
    return  Array.isArray(variable) ? true : false;
}



module.exports.isSomeEmpty = isSomeEmpty;
module.exports.isArray = isArray;
module.exports.isNullOrUndefined = isNullOrUndefined;
