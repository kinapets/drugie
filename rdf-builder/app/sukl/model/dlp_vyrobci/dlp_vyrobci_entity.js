
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_vyrobci {
    constructor(kod_sukl, vyr,zem){
        if (jsutils.isSomeEmpty([kod_sukl, vyr])) {
            throw new Error("class: all variables in costructor are mandatory" + kod_sukl + " " + vyr + " " + zem)
        }
        this.kod_sukl = kod_sukl;
        this.vyr = vyr;
        this.zem = zem;

    }

}

module.exports = Dlp_vyrobci;