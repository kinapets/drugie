/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import InteractionPairImage from "./interaction_pair_image";

export default class InteractionPairList extends Component {
    constructor(props){
        super(props);
        this.state = {
            activeItems: 3,
        };
    }


    render() {
        const NUMBER_PAIRS = this.props.interactionPairList.length;
        return (
            <ul className="list-group">
                {this.renderList()}
                <li className="list-group-item list-group-item-info">
                       <button
                           className="btn btn-info"
                           onClick={this.onMoreClick.bind(this)}
                       >
                           Load more... {this.state.activeItems}/{NUMBER_PAIRS}
                       </button>
                </li>
            </ul>
        );
    }

    renderList() {
        const interactionPair = this.props.interactionPairList;
        return interactionPair.slice(0, this.state.activeItems).map((function (pair) {
            const interacting_drug = pair.interactionConcept[1];

            return (
                <li key={interacting_drug.sourceConceptItem.id} className="list-group-item">
                    <div className="row">
                        <div className="col-lg-2">
                            <InteractionPairImage drugBankUrl={interacting_drug.sourceConceptItem.url}/>
                        </div>
                        <div className="col-lg-10">
                            <div className="inner-results">
                                <h3><a href="#">{interacting_drug.minConceptItem.name}</a></h3>
                                <ul className="list-inline up-ul">
                                    <li><span className="label label-primary">{pair.severity}</span></li>
                                    <li><a href="#">{interacting_drug.sourceConceptItem.id}</a></li>
                                </ul>
                                <p>
                                    {pair.description}
                                </p>
                                <ul className="list-inline down-ul">
                                    <li>

                                    </li>
                                    <li><a target="_blank"
                                           href={interacting_drug.sourceConceptItem.url}>{interacting_drug.sourceConceptItem.url}</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </li>
            );
        }).bind(this))
    }

    onMoreClick(event) {
        event.preventDefault();
        this.setState({
            activeItems: this.state.activeItems + 3
        });
    }

}

