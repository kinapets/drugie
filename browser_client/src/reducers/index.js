import { combineReducers } from 'redux';
import {reducer as formReducer} from "redux-form";

import LangsReducer from "./reducer_langs";
import LecivePripravky from "./reducer_dlp_lecivepripravky";
import Registration from "./reducer_registration_form";
import Login from "./reducer_login";
import MyPharmacy from "./reducer_mypharmacy";
import Ratings from "./reducer_ratings";
import RatingForm from "./reducer_ratingform";


const rootReducer = combineReducers({
    langs: LangsReducer,
    dlp_lecivepripravky: LecivePripravky,
    form: formReducer,
    registration: Registration,
    login: Login,
    myPharmacy: MyPharmacy,
    ratings: Ratings,
    ratingform: RatingForm
});

export default rootReducer;
