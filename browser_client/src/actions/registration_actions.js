import axios from "axios";

export const REGISTRATION_FORM_SUBMITTED = "REGISTRATION_FORM_SUBMITTED";
export const REGISTRATION_REGISTRATION_SUCCEDED = "REGISTRATION_REGISTRATION_SUCCEDED";
export const REGISTRATION_CLEAR_STATE = "REGISTRATION_CLEAR_STATE";

const ROOT_URL = "http://localhost:3000/users";

/**
 * Akce Kdy se odešle stav formuláře
 * @returns {{type: formSubmitted, payload: boolean}}
 */
export function formSubmitted(isSubmitted) {
    return {
        type: REGISTRATION_FORM_SUBMITTED,
        payload: isSubmitted
    }
}
/**
 * Validace formuláře
 *
 * @param values
 * @returns {{}}
 */
export function validateForm(values) {
    const errors = {};

    if (!values.firstName) {
        errors.firstName = "Enter a firstName";
    }

    if (!values.lastName) {
        errors.lastName = "Enter lastname";
    }

    if (!values.email || !validateEmail(values.email)) {
        errors.email = "Enter some valid email";
    }

    if (!values.password) {
        errors.password = "Enter some password";
    }
    if (!values.password2) {
        errors.password2 = "Enter some password";
    }

    if (values.password && values.password2 && (values.password !== values.password2)) {
        errors.password2 = "Passwords are different";
    }

    return errors;
}

/**
 * Asynchronní servrová validace
 *
 * @param values
 * @returns {Promise}
 */
export function serverSideValidate(values){
    return new Promise((resolve, reject) => {
        axios
            .post(`${ROOT_URL}/isregistered`,{email: values.email})
            .then(function(response) {
                if(response.data.result) {
                    reject({email: 'Email is already used.'});
                }else{
                    resolve();
                }
            });
    });
}

/**
 * Odeslání informací z formuláře na server
 *
 * @param values
 */
export function register(values){
    const request = axios.post(`${ROOT_URL}/register`,values);

    return{
            type:REGISTRATION_REGISTRATION_SUCCEDED,
            payload: request
    }


}
/**
 * Původní stav kompoennty
 *
 * @returns {{type: string}}
 */
export function clearState(){
    return {
        type: REGISTRATION_CLEAR_STATE

    };
}



/**
 * Validace emailu
 *
 * @param email
 * @returns {boolean}
 */
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
