var fs = require("fs");
var iconv = require("iconv-lite");

/**
 * Convert encoding function. This function convert specific file to specific encoding and save it to specific folder.
 * @param path - path of file
 * @param path_to - path where to save
 * @param encoding_from - input encoding
 * @param encoding_to - output encoding
 * @param callback - callback function
 */
module.exports.convertEncoding = function(path,path_to, encoding_from, encoding_to, callback) {
    var data = fs.createReadStream(path);
    data.pipe(iconv.decodeStream(encoding_from))
        .pipe(iconv.encodeStream(encoding_to))
        .pipe(fs.createWriteStream(path_to));
    data.on("error",function(err){
        throw new Error("Bad path to file. "+ err);

    })
    data.on('end', function () {
        return callback(path_to);
    });

};
