/**
 * Created by radims on 07.04.16.
 */

import {
    REGISTRATION_FORM_SUBMITTED,
    REGISTRATION_REGISTRATION_SUCCEDED,
    REGISTRATION_CLEAR_STATE
} from "../actions/registration_actions";

const INITIAL_STATE = {
    form_submitted : false, // byl formular odeslan?
    succeded: null
};

export default function(state = INITIAL_STATE, action) {
    switch (action.type){
        case REGISTRATION_FORM_SUBMITTED:
            return {... state, form_submitted:action.payload}
        case REGISTRATION_REGISTRATION_SUCCEDED:
            return {
                ...state,
                succeded: action.payload.data.success
            }
        case REGISTRATION_CLEAR_STATE:
            return INITIAL_STATE;
        default:
            return state;
    }
};
