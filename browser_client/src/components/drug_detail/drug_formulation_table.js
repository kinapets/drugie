/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import translations from "../../services/translator/translator";
import {Panel} from "react-bootstrap";
import Loader from "../common/loader";

import {connect} from "react-redux";

class DrugFormulationTable extends Component {
    render() {
        const ACTIVE_LANG = this.props.langs.active;

        if(this.props.drug_formulation === null)
            return (
                <Panel collapsible header={`${translations.drug_formulation_table.table_title[ACTIVE_LANG]}`}>
                <Loader/>
                </Panel>
            );

        return (
            <Panel collapsible header={`${translations.drug_formulation_table.table_title[ACTIVE_LANG]}`}>
                <table className="table">
                    <tbody>
                    {this.renderFormulationList()}

                    </tbody>
                </table>
            </Panel>
        );
    }

    renderFormulationList(){
        const drug_formulation = this.props.drug_formulation;
        return drug_formulation.map(function(item){
            return (<tr key={item.nazev_inn.value}>
                <td>{item.nazev_cz.value}</td>
                <td>{item.nazev_en.value}</td>
                <td>{item.nazev_inn.value}</td>
            </tr>);
        })
    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        dlp_lecivepripravky: state.dlp_lecivepripravky,
        langs: state.langs
    };
}

export default connect(mapStateToProps, null)(DrugFormulationTable);