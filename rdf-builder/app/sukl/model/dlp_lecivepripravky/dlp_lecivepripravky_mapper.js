/**
 * Created by radims on 19.02.16.
 */
/**
 * Created by radims on 12.02.16.
 */

var Entity = require("./dlp_lecivepripravky_entity");


module.exports.create = function(line) {
    var arr = line.split(";");
    var entity = new Entity(
        //kod_sukl - 1
        arr[0],
        //h - 2
        arr[1],
        //nazev - 3
        arr[2],
        //doplnek - 4
        arr[3],
        //cesta - 5
        arr[4],
        //forma - 6
        arr[5],
        //baleni - 7
        arr[6],
        //sila - 8
        arr[7],
        // obal
        arr[8],
        //drz -
        arr[9],
        //zemdr
        arr[10],
        //akt_drz
        arr[11],
        //akt_zem
        arr[12],
        //reg
        arr[13],
        //v_platdo
        arr[14],
        //neomez
        arr[15],
        //uvadenido
        arr[16],
        //is_
        arr[17],
        //atc_who
        arr[18],
        //rc
        arr[19],
        //sdov
        arr[20],
        //sdov_dov
        arr[21],
        //sdov_zem
        arr[22],
        //reg_proc
        arr[23],
        //dddamnt_who
        arr[24],
        //dddun_who
        arr[25],
        //ddd
        arr[26],
        //WHO Index
        arr[27],
        //Seznam léčivých látek, oddělený čárkami
        arr[28],
        //Klasifikace typu výdeje léčivého přípravku
        arr[29],
        //Závislost - skupina omamných nebo psychotropních látek (návykové látky)
        arr[30],
        //Doping
        arr[31],
        //Označení látky s anabolickým a jiným hormonálním účinkem podle nařízení vlády 454/2009 Sb.-
        arr[32],
        //Přípravek byl dodáván do lékáren a zdr. zařízení v posledním 3 měsících
        arr[33],
        //Seznam EAN kódů, oddělený čárkami
        arr[34],
        //Braillovo písmo: schváleno (S), výjimka (V), nežádáno (mezera)
        arr[35],
        //Expirace
        arr[36],
        //Expirace typ: hodiny H, dny D, týdny W, měsíce M
        arr[37]

    );
    return entity;
};