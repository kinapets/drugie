/**
 * Created by radims on 28.03.16.
 */
var express = require('express');
var router = express.Router();
var stardog_api = require("../stardog_api/stardog_api");



/* GET home page. */
router.get('/findAllByNamePattern', function(req, res, next) {
    var querName = req.query.name || "";
    var limit = req.query.limit || null;
    var offset = req.query.offset || null;
    stardog_api.dlp_lecivepripravky().findAllByName(querName,limit, offset, function(data){
        res.json(data);
    });
});

router.get("/countByNamePattern", function (req, res) {
    var querName = req.query.name || "";
    stardog_api.dlp_lecivepripravky().countByName(querName, function(data){
        res.json(data);
    });

});

router.get("/countAllSimilarDrugByATC", function (req, res) {
    var querName = req.query.id || "";
    stardog_api.dlp_lecivepripravky().countAllSimilarDrugByATC(querName, function(data){
        res.json(data);
    });

});
/* GET home page. */
router.get('/findById', function(req, res, next) {
    var id = req.query.id || "";

    stardog_api.dlp_lecivepripravky().findById(id, function(data){
        res.json(data);
    });
});

/* GET home page. */
router.get('/findDrugFormulationByDrugId', function(req, res, next) {
    var id = req.query.id || "";

    stardog_api.dlp_lecivepripravky().findDrugFormulationByDrugId(id, function(data){
        res.json(data);
    });


});/* GET home page. */
router.get('/findAllSimilarDrugByATC', function(req, res) {
    var id = req.query.id || "";
    var limit = req.query.limit || null;
    var offset = req.query.offset || null;
    stardog_api.dlp_lecivepripravky().findAllSimilarDrugByATC(id,limit,offset, function(data){
        res.json(data);
    });
});

module.exports = router;
