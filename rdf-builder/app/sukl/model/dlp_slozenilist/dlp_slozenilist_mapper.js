/**
 * Created by radims on 12.02.16.
 */

var Entity = require("./dlp_slozenilist_entity");


module.exports.create = function(line) {
    var arr = line.split(";");
    var entity = new Entity(arr[0]);
    return entity;
};