var readline = require('linebyline');
var fileUtils = require("../../../modules/utils/file");
var config = require("../../../config");
const INPUT_ENCODING = "cp1250";
const OUPUT_ENCODING = "utf-8";

module.exports.mapAll = function (sukl_version_path,file_name, mapper,callback ) {

    if(!sukl_version_path || !file_name|| !mapper||!callback) {
        throw new Error("Function map all doesn t have all arguments: sukl_version_path, file_name, config, mapper");
    }

    fileUtils.convertEncoding(
        sukl_version_path + file_name,
        config.base_paths.app.temp.data.base + file_name,
        INPUT_ENCODING,
        OUPUT_ENCODING,
        readFile
    );

    function readFile(path) {
        var array = [];
        var  rl = readline(path);
        rl.on('line', function (line, lineCount, byteCount) {
                if (lineCount != 1) {
                    try{
                        array.push(mapper.create(line));
                    }catch (e){
                        console.log("Mapper must imeplement function create");
                        throw e;
                    };
                }
            })
            .on('error', function (e) {
                throw new Error(e);
            })
            .on('end', function () {
                callback(array);
            });

    }
};







