
/**
 * Created by radims on 12.02.16.
 */

var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_slozenipriznak {
    constructor(s, vyznam){
        if (jsutils.isSomeEmpty([s, vyznam])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.s = s;
        this.vyznam = vyznam;

    }

}

module.exports = Dlp_slozenipriznak;