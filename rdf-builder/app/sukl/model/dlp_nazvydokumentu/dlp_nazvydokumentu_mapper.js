/**
 * Created by radims on 06.02.16.
 */
var Entity = require("./dlp_nazvydokumentu_entity");


module.exports.create = function(line) {
    var arr = line.split(";");
    var entity = new Entity(
        //kod_sukl
        arr[0],
        //pil
        arr[1],
        //dat_roz_pil
        arr[2],
        //spc
        arr[3],
        //dat_roz_spc
        arr[4],
        //obal_text
        arr[5],
        //dat_roz_obal
        arr[6],
        //nr
        arr[7],
        //dat_npm_nr
        arr[8]

    );
    return entity;
};