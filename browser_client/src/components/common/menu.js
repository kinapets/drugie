/**
 * Created by radims on 27.03.16.
 */
import React,{Component, PropTypes} from "react";
import {connect} from "react-redux";
import {Link} from "react-router";


import {changeLang} from "../../actions/index";
import {logout} from "../../actions/login_actions";


import {
    ROUTE_CONTACT,
    ROUTE_INDEX,
    ROUTE_HELP,
    ROUTE_SEARCH,
    ROUTE_MYDRUGSTORE,
    ROUTE_LOGIN,
    ROUTE_REGISTRATION
} from "../../routes";

import Authorized from "../common/authorized";

import translations from "../../services/translator/translator";


class Menu extends Component {
    static contextTypes = {
        router: PropTypes.object
    }

    //Nastvení jazyka
    onLanguageClick(lang) {
        this.props.changeLang(lang);
    }

    onLogoutClick() {
        this.props.logout();
    }

    render() {
        //nastavení aktuálního jazyka
        const ACTIVE_LANG = this.props.langs.active;
        const username = this.props.loginReducer.user ? this.props.loginReducer.user.username : "";
        var router = this.context.router;
        return (

            <div className="header">
                {/* Topbar */}
                <div className="topbar">
                    <div className="container">
                        {/* Topbar Navigation */}
                        <ul className="loginbar pull-right">
                            <li>
                                <i className="fa fa-globe"/>
                                <a>{translations.menu.languages[ACTIVE_LANG]}</a>
                                <ul className="lenguages">
                                    <li className="active">
                                        <a href=""
                                           onClick={(event) => {event.preventDefault();this.onLanguageClick("en")}}>
                                            {translations.menu.english[ACTIVE_LANG]}
                                            {this.props.langs.active === "en" ? <i className="fa fa-check"/> : ""}
                                        </a>
                                    </li>
                                    <li>
                                        <a href=""
                                           onClick={(event) => {event.preventDefault();this.onLanguageClick("cz")}}
                                        >
                                            {translations.menu.czech[ACTIVE_LANG]}
                                            {this.props.langs.active === "cz" ? <i className="fa fa-check"/> : ""}
                                        </a>

                                    </li>
                                </ul>
                            </li>
                            <Authorized authorize={true}><li className="topbar-devider"/>
                            </Authorized>
                            <Authorized authorize={true}>
                                <li>
                                    <a href="#">
                                        <i className="fa fa-user"></i>
                                        {username}
                                    </a>
                                </li>
                            </Authorized>
                            <Authorized authorize={false}>
                                <li className="topbar-devider"/>
                            </Authorized>
                            <li>
                                <Authorized authorize={false}>
                                    <Link to={ROUTE_REGISTRATION}>{translations.menu.registration[ACTIVE_LANG]}</Link>
                                </Authorized>
                            </li>
                            <Authorized authorize={false}>
                                <li className="topbar-devider"/>
                            </Authorized>
                            <li>
                                <Authorized authorize={false}>
                                    <Link to={ROUTE_LOGIN}>{translations.menu.login[ACTIVE_LANG]}</Link>
                                </Authorized>
                            </li>

                            <Authorized authorize={true}>
                                <li className="topbar-devider"/>
                            </Authorized>
                            <li>
                                <Authorized authorize={true}>
                                <a href="#"
                                   onClick={this.onLogoutClick.bind(this)}>{translations.menu.logout[ACTIVE_LANG]}</a>
                                </Authorized>
                            </li>
                        </ul>
                        {/* End Topbar Navigation */}
                    </div>
                </div>
                {/* End Topbar */}
                {/* Navbar */}
                <div className="navbar navbar-default" role="navigation">
                    <div className="container">
                        {/* Brand and toggle get grouped for better mobile display */}
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-responsive-collapse">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="fa fa-bars"/>
                            </button>

                            <Link className="navbar-brand" to={ROUTE_INDEX}>
                                <img id="logo-header" src="/img/logo_header.png" alt="Logo"/>
                            </Link>
                        </div>
                        {/* Collect the nav links, forms, and other content for toggling */}
                        <div className="collapse navbar-collapse navbar-responsive-collapse">
                            <ul className="nav navbar-nav">
                                {/* Home */}
                                <li>
                                    <Link to={ROUTE_INDEX}>
                                        {translations.menu.about[ACTIVE_LANG]}
                                    </Link>
                                </li>

                                <li className={router.isActive(ROUTE_SEARCH) ? "active": "" }>
                                    <Link to={ROUTE_SEARCH}>
                                        {translations.menu.drug_search[ACTIVE_LANG]}
                                    </Link>
                                </li>

                                <li className={router.isActive(ROUTE_MYDRUGSTORE) ? "active": "" }>
                                    <Link to={ROUTE_MYDRUGSTORE}>
                                        {translations.menu.my_drugstore[ACTIVE_LANG]}
                                    </Link>
                                </li>
                                <li className={router.isActive(ROUTE_HELP) ? "active": "" }>
                                    <Link to={ROUTE_HELP}>
                                        {translations.menu.help[ACTIVE_LANG]}
                                    </Link>
                                </li>
                                <li className={router.isActive(ROUTE_CONTACT) ? "active": "" }>
                                    <Link to={ROUTE_CONTACT}>
                                        {translations.menu.contact[ACTIVE_LANG]}
                                    </Link>
                                </li>
                            </ul>
                        </div>
                        {/*/navbar-collapse*/}
                    </div>
                </div>
                {/* End Navbar */}
            </div>

        );
    }
}


//namapování stavu do props
function mapStateToProps(state) {
    return {
        loginReducer: state.login,
        langs: state.langs
    };
}

//stačí tohle
export default connect(mapStateToProps, {changeLang, logout})(Menu);