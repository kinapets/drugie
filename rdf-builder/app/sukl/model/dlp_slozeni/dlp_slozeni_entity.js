/**
 * Created by radims on 05.03.16.
 */

var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_slozeni_entity {
    constructor(kod_sukl,kod_latky,sq,s,amnt_od,amnt,un){
        if (jsutils.isSomeEmpty([kod_sukl,kod_latky,sq,s,amnt_od,amnt,un])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.kod_sukl  = kod_sukl;
        this.kod_latky = kod_latky;
        this.sq= sq;
        this.s = s;
        this.amnt_od = amnt_od;
        this.amnt = amnt;
        this.un = un;
    }

}

module.exports = Dlp_slozeni_entity;