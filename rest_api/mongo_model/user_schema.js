/**
 * Created by radims on 13.04.16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');
var passwordHash = require('password-hash');


const DRUG_COLLECTION_NAME = require("./drug_schema").COLLECTION_NAME;
const COLLECTION_NAME = "Users";

var userSchema = new Schema({
    username: {
        type: String,
        unique: true
    },
    firstName: String,
    lastName: String,
    password: String,
    accessToken: {
        type:String,
        index: true
    },
    pharmacy: {
        type: Schema.Types.ObjectId,
        ref:DRUG_COLLECTION_NAME
    },

});

/**
 * Vrací uživatele na základě zadaného username
 *
 * @param username
 * @param callback
 * @returns {*|Query}
 */
userSchema.statics.findByUsername = function(username, callback) {
    return this.findOne({username: username}, callback);
};

/**
 * Hledání uživatele na základě access tokenu
 *
 * @param accessToken
 * @param callback
 * @returns {*|Query}
 */
userSchema.statics.findByAccessToken = function(accessToken, callback) {
    return this.findOne({accessToken:accessToken},callback);
};

/**
 * vygenerování a uložení nového access tokenu
 * @param callback
 */
userSchema.methods.generateNewAccessToken = function (callback) {
    this.accessToken = passwordHash.generate(String(Date.now()/ 1000));
    console.log(this);
    this.save(function (err,user) {
        callback(err,user);
    });

}


userSchema.plugin(uniqueValidator);

module.exports.Model = global.db.model(COLLECTION_NAME,userSchema);
module.exports.COLLECTION_NAME = COLLECTION_NAME;