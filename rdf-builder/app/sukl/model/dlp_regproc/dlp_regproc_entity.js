
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_regproc {
    constructor(regproc, nazev){
        if (jsutils.isSomeEmpty([regproc, nazev])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.reg_proc = regproc;
        this.nazev = nazev;

    }

}

module.exports = Dlp_regproc;