/**
 * Created by radims on 24.01.16.
 */
var Atc = require("./atc_entity.js");

/**
 * creates object of atc_entity
 * @param line
 * @returns {Atc|exports|module.exports}
 */
module.exports.create = function(line){
    var arr = line.split(";");
    var atc = new Atc(arr[0], arr[1], arr[2], arr[3]);

    return atc;
};


module.exports.createObject = function(line){
    var arr = line.split(";");
    var atc = new Atc(arr[0], arr[1], arr[2], arr[3]);

    return {
        atc: atc.getAtc()
    };
};
