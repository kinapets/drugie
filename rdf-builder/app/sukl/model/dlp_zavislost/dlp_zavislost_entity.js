
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_zavislost {
    constructor(zav, nazev_cs){
        if (jsutils.isSomeEmpty([zav, nazev_cs])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.zav = zav;
        this.nazev_cs = nazev_cs;

    }

}

module.exports = Dlp_zavislost;