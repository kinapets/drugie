/**
 * Created by radims on 12.02.16.
 */

var Entity = require("./dlp_vydej_entity");


module.exports.create = function(line) {
    var arr = line.split(";");
    var entity = new Entity(arr[0], arr[1]);
    return entity;
};