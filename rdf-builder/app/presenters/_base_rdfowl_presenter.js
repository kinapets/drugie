/**
 * Created by radims on 29.01.16.
 */

var SuklVersion = require("../../app/sukl/versions/versions");
var base_presenter = require("./_base_presenter");

/**
 * Basic rdf mapper - create object from csv file and fill template
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 *      params[2] - name file output
 * @param config - global app parameters
 * @param template_path - relative template path from base template path folder
 * @param file_name - name of csv file
 * @param mapper - creates object from line of file
 * @param fileext - ".rdf" or ".owl"
 */
module.exports.defaultAction = function (params,config,template_path,file_name,mapper,fileext,callback) {
    //setting variable from arguments
    var version = params[1] || null;
    var file_output = params[2] || null;

    //create new sukl version
    var sukl_version = new SuklVersion(config.base_paths.project.data.sukl.base);

    //define version of sukl
    if(version)
        var sukl_version_path = sukl_version.getVersion(version).directory;
    else
        var sukl_version_path = sukl_version.getLatestVersion().directory;

    var template_path = config.base_paths.app.app_folder.presenters.templates.base + template_path;

    var output_file_name = (file_output ? file_output + fileext : file_name + fileext )
    var output_template_path = config.base_paths.app.temp.data.sukl + output_file_name;

    base_presenter.render(
        sukl_version_path,
        file_name,
        template_path,
        output_template_path,
        mapper,
        {
            owl_uris: config.rdf_uris.owl,
            rdf_uris: config.rdf_uris.rdf,
            headers: config.owl_headers
        },callback
    );

};




