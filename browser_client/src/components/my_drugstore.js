import React from 'react';
import { Component } from 'react';
import Menu from "./common/menu";
import Breadcrumb from "./common/breadcrumb";
import {connect} from "react-redux";
import {Link} from "react-router";
import translations from "../services/translator/translator";
import {ROUTE_DETAILDRUG} from "../routes";
import {fetchMyPharmacy} from "../actions/mypharmacy_actions";
import Authorized  from "../components/common/authorized";
import {Panel,Pagination} from "react-bootstrap";


class MyDrugstore extends Component {


    componentDidUpdate() {
        const access_token = this.props.loginReducer.accessToken;
        const isFetched = this.props.myPharmacy.fetched;

        if (access_token && !isFetched)
            this.props.fetchMyPharmacy(access_token);
    }

    render() {
        const ACTIVE_LANG = this.props.langs.active;
        return (
            <div>
                <Menu/>
                <Breadcrumb
                    title={`${translations.drugstore.breadcrumb_title[ACTIVE_LANG]}`}
                    pages={[{linkTo:"/", title: "Drugie"},]}
                    last={translations.drugstore.breadcrumb_page[ACTIVE_LANG]}
                />
                <div className="container">

                <Panel collapsible defaultExpanded  header={`${translations.drugstore.panel_title[ACTIVE_LANG]}`}>
                <table className="table">
                    <tbody>
                        {this.renderMyPharmacy()}
                    </tbody>
                </table>
                </Panel>
                </div>
            </div>
        );
    }

    renderMyPharmacy() {
        const drug_list = this.props.myPharmacy.drug_list;

        return drug_list.map(function (item) {

            return (
                <tr key={item.kod_sukl.value}>
                    <td>
                        <Link to={`${ROUTE_DETAILDRUG}/${item.kod_sukl.value}`}>
                            {item.kod_sukl.value}
                        </Link>
                    </td>
                    <td>{item.nazev.value}</td>
                    <td>{item.sila.value}</td>
                    <td ><span className="pull-right">
                        <Link className="btn btn-default"   to={`${ROUTE_DETAILDRUG}/${item.kod_sukl.value}`}>
                            <i className="fa fa-list"></i>
                        </Link></span>
                    </td>
                </tr>
            );
        });
    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        langs: state.langs,
        myPharmacy: state.myPharmacy,
        loginReducer: state.login
    };
}

const actions = {fetchMyPharmacy}

export default connect(mapStateToProps, actions)(MyDrugstore);