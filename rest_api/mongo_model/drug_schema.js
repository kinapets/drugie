/**
 * Created by radims on 13.04.16.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


const USER_COLLECTION_NAME = require("./user_schema").COLLECTION_NAME;
const COLLECTION_NAME = "Drugs";

var drugSchema = new Schema({
    nazev:{
        value: String,
    },
    kod_sukl:{
        value: {
            type:String,
            index: true
        },
    },
    sila:{
        value:{
            type: String,
        }
    },
    doplnek:{
        value:{
            type:String
        }
    },
    ratings: [{
        rating: Number,
        user: {
            type: Schema.Types.ObjectId,
            ref: USER_COLLECTION_NAME
        },
        comment: String,
    }]
});

module.exports.Model = global.db.model(COLLECTION_NAME,drugSchema);
module.exports.COLLECTION_NAME = COLLECTION_NAME;
