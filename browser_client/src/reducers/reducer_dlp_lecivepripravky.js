/**
 * Created by radims on 27.03.16.
 */
import {
    FETCH_DRUGS_BY_NAME,
    CHANGE_QUERY_WORD,
    FETCH_DRUG_BY_ID,
    FETCH_DRUG_COUNT_NAME,
    CLEAR_DETAIL,
    FETCH_PRICES_DRUG_BY_ID,
    FETCH_DRUG_FORMULATION_BY_DRUG_ID,
    FETCH_ALL_SIMILAR_DRUG_BY_ATC,
    CHANGE_PAGE_SIMILAR_DRUGS,
    FETCH_COUNT_ALL_SIMILAR_DRUG_BY_ATC,
    FETCH_INTERACTION_BY_ID
} from "../actions/index";

const INITIAL_STATE = {
    search_list: [], //výpis léků ve vyhledávání
    loading: false, //zachycení stavu načítání léků
    detail_drug: null, //lék u kterého chceme ukázat detail
    drug_formulation:null,//složení léku
    detail_drug_prices: null, //ceny detailu daného léku
    detail_drug_similar: null,//podobné léky
    detail_drug_similar_page: 0, //stránkování v podobných lécích
    detail_drug_similar_count: 0, //počet podobných léků
    detail_drug_interaction_list: null,
    search_list_query: {
        search_word: "",
        page: 0,
    },
    search_result_count: null //počet výsledů

};

export default function(state = INITIAL_STATE, action) {
    switch (action.type){
        case FETCH_DRUGS_BY_NAME:
            return {
                ... state,
                detail_drug:null,
                search_list:action.payload.data,
                loading:false
            }
        case FETCH_DRUG_BY_ID:
            return {
                ... state,
                detail_drug:action.payload.data
            }
        case CHANGE_QUERY_WORD:
            return {
                ... state,
                search_list_query: action.payload,
                loading:true
            }
        case FETCH_DRUG_COUNT_NAME:
            return {
                ...state,
                search_result_count:action.payload.data[0].count.value
            }
        case FETCH_PRICES_DRUG_BY_ID:
            return {
                ...state,
                detail_drug_prices: action.payload.data
            }
        case FETCH_DRUG_FORMULATION_BY_DRUG_ID:
            return {
                ... state,
                drug_formulation: action.payload.data
            }
        case CLEAR_DETAIL:
            return {
                ...state,
                detail_drug: null,
                detail_drug_prices: null,
                detail_drug_similar: null,
                detail_drug_similar_page: 0,
                detail_drug_interaction_list: null,
                drug_formulation: null
            }
        case FETCH_ALL_SIMILAR_DRUG_BY_ATC:
            return {
                ...state,
                detail_drug_similar: action.payload.data
            }
        case CHANGE_PAGE_SIMILAR_DRUGS:
            return{
                ...state,
                detail_drug_similar_page: action.payload
            }
        case FETCH_COUNT_ALL_SIMILAR_DRUG_BY_ATC:
            return {
                ...state,
                detail_drug_similar_count:action.payload.data[0].count.value
            }
        case FETCH_INTERACTION_BY_ID:
            return {
                ...state,
                detail_drug_interaction_list:  action.payload.data.interactionTypeGroup

            }
        default:
            return state;
    }
};