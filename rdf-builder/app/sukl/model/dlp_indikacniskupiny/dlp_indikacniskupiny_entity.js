/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_indikacniskupiny_entity {
    constructor(indsk, nazev){
        if (jsutils.isSomeEmpty([indsk, nazev])) {
            throw new Error("class dlp_cesty_entity: all variables in costructor are mandatory")
        }
        this.indsk = indsk;
        this.nazev = nazev;
    }

}

module.exports = Dlp_indikacniskupiny_entity;