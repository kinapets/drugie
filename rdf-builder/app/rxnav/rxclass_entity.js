class Rxclass{

    constructor(){
        this.minConceptRxcui = null;
        this.minConceptName = null;
        this.minConceptTty = null;
        this.sourceId = null;
        this.sourceName = null;
        this.relation = null;
    }

    setMinConceptRxcui(minConceptRxcui) { this.minConceptRxcui = minConceptRxcui; }
    getMinConceptRxcui() { return this.minConceptRxcui; }
    setMinConceptName(minConceptName) { this.minConceptName = minConceptName; }
    getMinConceptName() { return this.minConceptName; }
    setMinConceptTty(minConceptTty) { this.minConceptTty = minConceptTty; }
    getMinConceptTty() { return this.minConceptTty; }
    setSourceId(sourceId) { this.sourceId = sourceId; }
    getSourceId() { return this.sourceId; }
    setSourceName(sourceName) { this.sourceName = sourceName; }
    getSourceName() { return this.sourceName; }
    setRelation(relation) { this.relation = relation; }
    getRelation() { return this.relation; }

}

module.exports = Rxclass;

