/**
 * Created by radims on 05.03.16.
 */
/**
 * Created by radims on 12.02.16.
 */

var Entity = require("./dlp_slozeni_entity");


module.exports.create = function(line) {
    var arr = line.split(";");
    var entity = new Entity(
        //kod_sukl
        arr[0],
        // kod_latky
        arr[1],
        // sq
        arr[2],
        // s
        arr[3],
        // amnt_od
        arr[4],
        // amnt
        arr[5],
        // un
        arr[6]
    );
    return entity;
};