/**
 * Created by radims on 29.01.16.
 */


var atcMapper = require("../sukl/model/atc/atc_mapper");
var owlrdf_presenter = require("./_base_rdfowl_presenter");


const TEMPLATE_PATH_DEFAULT = "atc/default.html";
const TEMPLATE_PATH_RDF = "atc/rdf.html";

/**
 * Generate owl file of ATC
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters
 */
module.exports.defaultAction = function(params, config, callback) {
    callback = callback || console.log;
    owlrdf_presenter.defaultAction(
        params,
        config,
        TEMPLATE_PATH_DEFAULT,
        config.file_names.sukl.dlp_atc,
        atcMapper,
        ".owl",
        callback
    );
};


/**
 * Generate rdf file of ATC
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters
 */
module.exports.rdfAction = function(params, config,callback) {
    callback = callback || console.log;
    owlrdf_presenter.defaultAction(
        params,
        config,
        TEMPLATE_PATH_RDF,
        config.file_names.sukl.dlp_atc,
        atcMapper,
        ".rdf",
        callback
    );
};



