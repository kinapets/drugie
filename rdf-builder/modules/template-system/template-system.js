/**
 * Created by radims on 25.01.16.
 */
var Mustache = require("mustache");
var fs = require("fs");

/**
 * Template system function. The function render some document and save it in specific folder.
 * @param template_path - path of template
 * @param output_path - path, where we want to save document
 * @param template_vars - template variables
 * @param callback - callback function
 */
var render = function(template_path,output_path, template_vars,callback) {
    fs.readFile(template_path, function (err, data) {
        if(err) {
            throw new Error(err);
        }
        if(data) {
            var output = Mustache.render(data.toString(), template_vars);

            fs.writeFile(output_path, output,function(err) {
                if(err) throw err;
                console.log("Done:" + output_path);
                if(callback)
                    callback(output);

            } )

        }
    });
};


module.exports.render = render;