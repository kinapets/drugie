/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';

export default class Loader extends Component {
    render() {
        if(this.props.widthLoader)
            return (
                <div className="text-align-center">
                    <img src="/img/loader.gif" width={this.props.widthLoader}/>
                </div>
            );
        else
            return (
                <div className="text-align-center">
                    <img src="/img/loader.gif"/>
                </div>
            );

    }
}

