/**
 * Created by radims on 24.01.16.
 */
var Entity = require("./dlp_cesty_entity.js");

/**
 * creates object of atc_entity
 * @param line
 * @returns {Atc|exports|module.exports}
 */
module.exports.create = function(line){
    var arr = line.split(";");
    var entity = new Entity(arr[0], arr[1], arr[2], arr[3]);

    return entity;
};

