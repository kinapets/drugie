/**
 * Created by radims on 06.02.16.
 */
var Entity = require("./dlp_latky_entity");


module.exports.create = function(line) {
    var arr = line.split(";");
    var entity = new Entity(arr[0], arr[1],arr[2],arr[3],arr[4],arr[5],arr[6],arr[7]);
    return entity;
};