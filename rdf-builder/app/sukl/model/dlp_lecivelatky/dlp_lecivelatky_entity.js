
/**
 * Created by radims on 29.02.16.
 */

var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_lecivelatky_entity{
    constructor(kod_latky,nazev_inn,nazev_en,nazev){
        if (jsutils.isSomeEmpty([kod_latky,nazev_inn,nazev_en,nazev])) {
            throw new Error("class dlp_cesty_entity: all variables in costructor are mandatory")
        }
        this.kod_latky = kod_latky;
        this.nazev_inn = nazev_inn;
        this.nazev_en = nazev_en;
        this.nazev = nazev;
    }

}

module.exports = Dlp_lecivelatky_entity;