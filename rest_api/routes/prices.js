/**
 * Created by radims on 03.04.16.
 */
/* GET home page. */
var express = require('express');
var router = express.Router();
var request = require("request");
var cheerio = require("cheerio");

var API_REQUEST = "http://www.mzcr.cz/leky.aspx?otevri=true&naz=";


String.prototype.hashCode = function() {
    var hash = 0, i, chr, len;
    if (this.length === 0) return hash;
    for (i = 0, len = this.length; i < len; i++) {
        chr   = this.charCodeAt(i);
        hash  = ((hash << 5) - hash) - chr;
        hash |= 0; // Convert to 32bit integer
    }
    return Math.abs(hash);
};

/**
 * Dotáže na stránky ministerstva a stáhne a rozparsuje tabulkz
 */
router.get('/findById', function(req, res, next) {
    const id = req.query.id;

    request(`${API_REQUEST}&lek=${id}`, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            $ = cheerio.load(body);
            var arr = [];
            $('.tabulka tr').each(function() {
                if($(this).text().indexOf("Kč")== -1)
                    return;
                tempArr = $(this).text().trim().replace(/(\r\n|\n|\r|\t)/gm,"").replace("KčA","Kč").split(":");
                arr.push({title:tempArr[0],titleHash:tempArr[0].hashCode(),value: tempArr[1]});

            });
            res.json(arr) // Show the HTML for the Google homepage.
        }
    })
});

module.exports = router;