const APP_NAME_FOLDER = "rdf-builder/";
const PROJECT_BASE_PATH = "/Users/radims/dev/drug-allergy/";

const PROJECT_TEMP_PATH = "temp/";
const PROJECT_TEMP_DATA_FOLDER__NAME = "data/"
const PROJECT_TEMP_DATA_SUKL_FOLDER__NAME = "sukl/"

const TEMPLATES_FOLDER_NAME = "app/presenters/templates/";
const PRESENTERS_FOLDER_NAME = "app/presenters/";

const DATA_FOLDER_NAME = "data/";
const DATA_SUKL_FOLDER_NAME = "sukl/";

const FILE_NAMES_SUKL_ATC = "dlp_atc.csv";


const FILE_NAMES_SUKL_DLP_CESTY = "dlp_cesty.csv";



const RDF_URIS_BASE = "http://db.cz/"
const RDF_OWL_URIS_BASE = "owl/";

//RDF_OWL_URIS_BASE
const RDF_OWL_ATC_URIS_BASE = "atc/";


const RDF_DATATYPE_PROPERTY_N = "n/";
const RDF_DATATYPE_PROPERTY_NAME_EN = "name_en/";
const RDF_DATATYPE_PROPERTY_NAME = "name/";
const RDF_DATATYPE_ATC = "atc/";


const RDF_URI_BASE = "rdf/";
const RDF_ATC_URIS_BASE = "atc/";

const OWL_HEADERS_ATC_TITLE = "ATC Ontology";
const OWL_HEADERS_ATC_DESCRIPTION = "Description of atc ontology. ";
const OWL_HEADERS_ATC_BASE_CLASS_LABEL = "Base of atc ontology";
const OWL_HEADERS_ATC_BASE_CLASS_COMMENT = "Root of atc structure";




var config = {};
//================================== BASE PATHS ==========================================
config.base_paths = {};
//project base path
config.base_paths.project = {};
config.base_paths.project.base =PROJECT_BASE_PATH;
//data base path
config.base_paths.project.data = {};
config.base_paths.project.data.base = config.base_paths.project.base + DATA_FOLDER_NAME;
config.base_paths.project.data.sukl =  {};
config.base_paths.project.data.sukl.base = config.base_paths.project.data.base + DATA_SUKL_FOLDER_NAME;
config.base_paths.project.data.sukl.files = {};
config.base_paths.project.data.sukl.files.dlp_atc = config.base_paths.project.data.sukl.base + FILE_NAMES_SUKL_ATC;

//app base path
config.base_paths.app = {};
config.base_paths.app.base = config.base_paths.project.base + APP_NAME_FOLDER;

config.base_paths.app.app_folder = {}
config.base_paths.app.app_folder.presenters = {};
config.base_paths.app.app_folder.presenters.templates = {};
config.base_paths.app.app_folder.presenters.templates.base = config.base_paths.app.base + TEMPLATES_FOLDER_NAME;
config.base_paths.app.app_folder.presenters.base = config.base_paths.app.base + PRESENTERS_FOLDER_NAME;




//temp base path
config.base_paths.app.temp = {};
config.base_paths.app.temp.base = config.base_paths.app.base+ PROJECT_TEMP_PATH;
config.base_paths.app.temp.data = {};
config.base_paths.app.temp.data.base = config.base_paths.app.temp.base + PROJECT_TEMP_DATA_FOLDER__NAME;
config.base_paths.app.temp.data.sukl = config.base_paths.app.temp.data.base + PROJECT_TEMP_DATA_SUKL_FOLDER__NAME;
config.base_paths.app.temp.data.rxnav = config.base_paths.app.temp.data.base + "rxnav/";

//================================== File names ==========================================
config.file_names = {};
config.file_names.sukl = {};
//ATC
config.file_names.sukl.dlp_atc = FILE_NAMES_SUKL_ATC;
//dlp_cesty
config.file_names.sukl.dlp_cesty = FILE_NAMES_SUKL_DLP_CESTY;
config.file_names.sukl.dlp_doping = "dlp_doping.csv";
config.file_names.sukl.dlp_formy = "dlp_formy.csv";
config.file_names.sukl.dlp_indikacniskupiny = "dlp_indikacniskupiny.csv";
config.file_names.sukl.dlp_jednotky = "dlp_jednotky.csv";
config.file_names.sukl.dlp_latky = "dlp_latky.csv";
config.file_names.sukl.dlp_lecivelatky = "dlp_lecivelatky.csv";
config.file_names.sukl.dlp_lecivepripravky = "dlp_lecivepripravky.csv";
config.file_names.sukl.dlp_narvla = "dlp_narvla.csv";
config.file_names.sukl.dlp_nazvydokumentu = "dlp_nazvydokumentu.csv";
config.file_names.sukl.dlp_obaly = "dlp_obaly.csv";
config.file_names.sukl.dlp_organizace = "dlp_organizace.csv";
config.file_names.sukl.dlp_regproc = "dlp_regproc.csv";
config.file_names.sukl.dlp_slozeni = "dlp_slozeni.csv";
config.file_names.sukl.dlp_slozenilist = "dlp_lecivepripravky.csv";
config.file_names.sukl.dlp_soli = "dlp_soli.csv";
config.file_names.sukl.dlp_splp = "dlp_splp.csv";
config.file_names.sukl.dlp_stavyreg = "dlp_stavyreg.csv";
config.file_names.sukl.dlp_synonyma = "dlp_synonyma.csv";
config.file_names.sukl.dlp_slozenipriznak = "dlp_slozenipriznak.csv";
config.file_names.sukl.dlp_vpois = "dlp_vpois.csv";
config.file_names.sukl.dlp_vydej = "dlp_vydej.csv";
config.file_names.sukl.dlp_vyrobci = "dlp_vyrobci.csv";
config.file_names.sukl.dlp_zavislost = "dlp_zavislost.csv";
config.file_names.sukl.dlp_zdroje = "dlp_zdroje.csv";
config.file_names.sukl.dlp_zeme = "dlp_zeme.csv";


//




//================================== OWL - URIs ==============================================
config.rdf_uris = {};
config.rdf_uris.base = RDF_URIS_BASE;
config.rdf_uris.owl = {};
config.rdf_uris.owl.base = config.rdf_uris.base + RDF_OWL_URIS_BASE;
config.rdf_uris.rdf = {};
config.rdf_uris.rdf.base = config.rdf_uris.base + RDF_URI_BASE;
config.owl_headers = {};
//atc
config.rdf_uris.owl.atc = {};
config.rdf_uris.owl.atc.base = config.rdf_uris.owl.base + RDF_OWL_ATC_URIS_BASE;
config.rdf_uris.owl.atc.datatype_property_n = config.rdf_uris.owl.atc.base + RDF_DATATYPE_PROPERTY_N;
config.rdf_uris.owl.atc.datatype_property_atc = config.rdf_uris.owl.atc.base + RDF_DATATYPE_ATC;
config.rdf_uris.owl.atc.datatype_property_name = config.rdf_uris.owl.atc.base + RDF_DATATYPE_PROPERTY_NAME;
config.rdf_uris.owl.atc.datatype_property_name_en = config.rdf_uris.owl.atc.base + RDF_DATATYPE_PROPERTY_NAME_EN;

config.rdf_uris.rdf.atc = {};
config.rdf_uris.rdf.atc.base = config.rdf_uris.rdf.base + "atc/";

config.owl_headers.atc = {};
config.owl_headers.atc.ontology_title = OWL_HEADERS_ATC_TITLE;
config.owl_headers.atc.ontology_description = OWL_HEADERS_ATC_DESCRIPTION;
config.owl_headers.atc.base_class_label = OWL_HEADERS_ATC_BASE_CLASS_LABEL;
config.owl_headers.atc.base_class_comment = OWL_HEADERS_ATC_BASE_CLASS_COMMENT;


//dlp_cesty
config.rdf_uris.owl.dlp_cesty = {};
config.rdf_uris.owl.dlp_cesty.base = config.rdf_uris.owl.base + "dlp_cesty/";
config.rdf_uris.owl.dlp_cesty.datatype_property_cesta = config.rdf_uris.owl.dlp_cesty.base + "cesta";
config.rdf_uris.owl.dlp_cesty.datatype_property_nazev = config.rdf_uris.owl.dlp_cesty.base + "nazev";
config.rdf_uris.owl.dlp_cesty.datatype_property_nazev_en = config.rdf_uris.owl.dlp_cesty.base + "nazev_en";
config.rdf_uris.owl.dlp_cesty.datatype_property_nazev_lat = config.rdf_uris.owl.dlp_cesty.base + "nazev_lat";

config.rdf_uris.rdf.dlp_cesty = {};
config.rdf_uris.rdf.dlp_cesty.base = config.rdf_uris.rdf.base + "dlp_cesty/";

config.owl_headers.dlp_cesty = {};
config.owl_headers.dlp_cesty.ontology_title = "Cesty podání";
config.owl_headers.dlp_cesty.ontology_description = "Cesty podání";
config.owl_headers.dlp_cesty.base_class_label = "Cesty podání";
config.owl_headers.dlp_cesty.base_class_comment = "Cesty podání";

//dlp_doping
config.rdf_uris.owl.dlp_doping = {};
config.rdf_uris.owl.dlp_doping.base = config.rdf_uris.owl.base + "dlp_doping/";
config.rdf_uris.owl.dlp_doping.datatype_property_doping = config.rdf_uris.owl.dlp_doping.base + "doping";
config.rdf_uris.owl.dlp_doping.datatype_property_nazev = config.rdf_uris.owl.dlp_doping.base + "nazev";


config.rdf_uris.rdf.dlp_doping = {};
config.rdf_uris.rdf.dlp_doping.base = config.rdf_uris.rdf.base + "dlp_doping/";

config.owl_headers.dlp_doping = {};
config.owl_headers.dlp_doping.ontology_title = "Doping";
config.owl_headers.dlp_doping.ontology_description = "Doping";
config.owl_headers.dlp_doping.base_class_label = "Doping";
config.owl_headers.dlp_doping.base_class_comment = "Doping";

//dlp_formy
config.rdf_uris.owl.dlp_formy = {};
config.rdf_uris.owl.dlp_formy.base = config.rdf_uris.owl.base + "dlp_formy/";
config.rdf_uris.owl.dlp_formy.datatype_property_cesta = config.rdf_uris.owl.dlp_formy.base + "cesta";
config.rdf_uris.owl.dlp_formy.datatype_property_forma = config.rdf_uris.owl.dlp_formy.base + "forma";
config.rdf_uris.owl.dlp_formy.datatype_property_nazev = config.rdf_uris.owl.dlp_formy.base + "nazev";
config.rdf_uris.owl.dlp_formy.datatype_property_nazev_en = config.rdf_uris.owl.dlp_formy.base + "nazev_en";
config.rdf_uris.owl.dlp_formy.datatype_property_nazev_lat = config.rdf_uris.owl.dlp_formy.base + "nazev_lat";


config.rdf_uris.rdf.dlp_formy = {};
config.rdf_uris.rdf.dlp_formy.base = config.rdf_uris.rdf.base + "dlp_formy/";

config.owl_headers.dlp_formy = {};
config.owl_headers.dlp_formy.ontology_title = "Formy";
config.owl_headers.dlp_formy.ontology_description = "Formy";
config.owl_headers.dlp_formy.base_class_label = "Formy";
config.owl_headers.dlp_formy.base_class_comment = "Formy";

//dlp_indikacniskupiny
config.rdf_uris.owl.dlp_indikacniskupiny = {};
config.rdf_uris.owl.dlp_indikacniskupiny.base = config.rdf_uris.owl.base + "dlp_indikacniskupiny/";
config.rdf_uris.owl.dlp_indikacniskupiny.datatype_property_indsk = config.rdf_uris.owl.dlp_indikacniskupiny.base + "indsk";
config.rdf_uris.owl.dlp_indikacniskupiny.datatype_property_nazev = config.rdf_uris.owl.dlp_indikacniskupiny.base + "nazev";



config.rdf_uris.rdf.dlp_indikacniskupiny = {};
config.rdf_uris.rdf.dlp_indikacniskupiny.base = config.rdf_uris.rdf.base + "dlp_indikacniskupiny/";

config.owl_headers.dlp_indikacniskupiny = {};
config.owl_headers.dlp_indikacniskupiny.ontology_title = "Indikační skupiny";
config.owl_headers.dlp_indikacniskupiny.ontology_description = "Indikační skupiny";
config.owl_headers.dlp_indikacniskupiny.base_class_label = "Indikační skupiny";
config.owl_headers.dlp_indikacniskupiny.base_class_comment = "Indikační skupiny";


//dlp_indikacniskupiny
config.rdf_uris.owl.dlp_jednotky = {};
config.rdf_uris.owl.dlp_jednotky.base = config.rdf_uris.owl.base + "dlp_jednotky/";
config.rdf_uris.owl.dlp_jednotky.datatype_property_un = config.rdf_uris.owl.dlp_jednotky.base + "un";
config.rdf_uris.owl.dlp_jednotky.datatype_property_nazev = config.rdf_uris.owl.dlp_jednotky.base + "nazev";


config.rdf_uris.rdf.dlp_jednotky = {};
config.rdf_uris.rdf.dlp_jednotky.base = config.rdf_uris.rdf.base + "dlp_jednotky/";

config.owl_headers.dlp_jednotky = {};
config.owl_headers.dlp_jednotky.ontology_title = "Jednotky";
config.owl_headers.dlp_jednotky.ontology_description = "Jednotky";
config.owl_headers.dlp_jednotky.base_class_label = "Jednotky";
config.owl_headers.dlp_jednotky.base_class_comment = "Jednotky";


//dlp_indikacniskupiny
config.rdf_uris.owl.dlp_narvla = {};
config.rdf_uris.owl.dlp_narvla.base = config.rdf_uris.owl.base + "dlp_narvla/";
config.rdf_uris.owl.dlp_narvla.datatype_property_narvla = config.rdf_uris.owl.dlp_narvla.base + "narvla";
config.rdf_uris.owl.dlp_narvla.datatype_property_nazev = config.rdf_uris.owl.dlp_narvla.base + "nazev";


config.rdf_uris.rdf.dlp_narvla = {};
config.rdf_uris.rdf.dlp_narvla.base = config.rdf_uris.rdf.base + "dlp_narvla/";

config.owl_headers.dlp_narvla = {};
config.owl_headers.dlp_narvla.ontology_title = "Narvla";
config.owl_headers.dlp_narvla.ontology_description = "Látky s anabolickým a jiným hormonálním účinkem - Nařízení vlády 454/2009 Sb.";
config.owl_headers.dlp_narvla.base_class_label = "Látky s anabolickým a jiným hormonálním účinkem - Nařízení vlády 454/2009 Sb.";
config.owl_headers.dlp_narvla.base_class_comment = "Látky s anabolickým a jiným hormonálním účinkem - Nařízení vlády 454/2009 Sb.";

//dlp_obaly
config.rdf_uris.owl.dlp_obaly = {};
config.rdf_uris.owl.dlp_obaly.base = config.rdf_uris.owl.base + "dlp_obaly/";
config.rdf_uris.owl.dlp_obaly.datatype_property_obal = config.rdf_uris.owl.dlp_obaly.base + "obal";
config.rdf_uris.owl.dlp_obaly.datatype_property_nazev = config.rdf_uris.owl.dlp_obaly.base + "nazev";
config.rdf_uris.owl.dlp_obaly.datatype_property_nazev_en = config.rdf_uris.owl.dlp_obaly.base + "nazev_en";


config.rdf_uris.rdf.dlp_obaly = {};
config.rdf_uris.rdf.dlp_obaly.base = config.rdf_uris.rdf.base + "dlp_obaly/";

config.owl_headers.dlp_obaly = {};
config.owl_headers.dlp_obaly.ontology_title = "Obaly";
config.owl_headers.dlp_obaly.ontology_description = "Obaly";
config.owl_headers.dlp_obaly.base_class_label = "Obaly";
config.owl_headers.dlp_obaly.base_class_comment = "Obaly";

//dlp_regproc
config.rdf_uris.owl.dlp_regproc = {};
config.rdf_uris.owl.dlp_regproc.base = config.rdf_uris.owl.base + "dlp_regproc/";
config.rdf_uris.owl.dlp_regproc.datatype_property_reg_proc = config.rdf_uris.owl.dlp_regproc.base + "reg_proc";
config.rdf_uris.owl.dlp_regproc.datatype_property_nazev = config.rdf_uris.owl.dlp_regproc.base + "nazev";



config.rdf_uris.rdf.dlp_regproc = {};
config.rdf_uris.rdf.dlp_regproc.base = config.rdf_uris.rdf.base + "dlp_regproc/";

config.owl_headers.dlp_regproc = {};
config.owl_headers.dlp_regproc.ontology_title = "Registrační procedura";
config.owl_headers.dlp_regproc.ontology_description = "Registrační procedura";
config.owl_headers.dlp_regproc.base_class_label = "Registrační procedura";
config.owl_headers.dlp_regproc.base_class_comment = "Registrační procedura";

//dlp_slozenipriznak
config.rdf_uris.owl.dlp_slozenipriznak = {};
config.rdf_uris.owl.dlp_slozenipriznak.base = config.rdf_uris.owl.base + "dlp_slozenipriznak/";
config.rdf_uris.owl.dlp_slozenipriznak.datatype_property_s = config.rdf_uris.owl.dlp_slozenipriznak.base + "s";
config.rdf_uris.owl.dlp_slozenipriznak.datatype_property_vyznam = config.rdf_uris.owl.dlp_slozenipriznak.base + "vyznam";

config.rdf_uris.rdf.dlp_slozenipriznak = {};
config.rdf_uris.rdf.dlp_slozenipriznak.base = config.rdf_uris.rdf.base + "dlp_slozenipriznak/";

config.owl_headers.dlp_slozenipriznak = {};
config.owl_headers.dlp_slozenipriznak.ontology_title = "Registrační procedura";
config.owl_headers.dlp_slozenipriznak.ontology_description = "Registrační procedura";
config.owl_headers.dlp_slozenipriznak.base_class_label = "Registrační procedura";
config.owl_headers.dlp_slozenipriznak.base_class_comment = "Registrační procedura";


//dlp_stavyreg
config.rdf_uris.owl.dlp_stavyreg = {};
config.rdf_uris.owl.dlp_stavyreg.base = config.rdf_uris.owl.base + "dlp_stavyreg/";
config.rdf_uris.owl.dlp_stavyreg.datatype_property_s = config.rdf_uris.owl.dlp_stavyreg.base + "reg";
config.rdf_uris.owl.dlp_stavyreg.datatype_property_nazev = config.rdf_uris.owl.dlp_stavyreg.base + "nazev";

config.rdf_uris.rdf.dlp_stavyreg = {};
config.rdf_uris.rdf.dlp_stavyreg.base = config.rdf_uris.rdf.base + "dlp_stavyreg/";

config.owl_headers.dlp_stavyreg = {};
config.owl_headers.dlp_stavyreg.ontology_title = "Stav registrace";
config.owl_headers.dlp_stavyreg.ontology_description = "Stav registrace";
config.owl_headers.dlp_stavyreg.base_class_label = "Stav registrace";
config.owl_headers.dlp_stavyreg.base_class_comment = "Stav registrace";

//dlp_vydej
config.rdf_uris.owl.dlp_vydej = {};
config.rdf_uris.owl.dlp_vydej.base = config.rdf_uris.owl.base + "dlp_vydej/";
config.rdf_uris.owl.dlp_vydej.datatype_property_vydej = config.rdf_uris.owl.dlp_vydej.base + "vydej";
config.rdf_uris.owl.dlp_vydej.datatype_property_nazev = config.rdf_uris.owl.dlp_vydej.base + "nazev";

config.rdf_uris.rdf.dlp_vydej = {};
config.rdf_uris.rdf.dlp_vydej.base = config.rdf_uris.rdf.base + "dlp_vydej/";

config.owl_headers.dlp_vydej = {};
config.owl_headers.dlp_vydej.ontology_title = "Typ výdeje léčivého přípravku";
config.owl_headers.dlp_vydej.ontology_description = "Typ výdeje léčivého přípravku";
config.owl_headers.dlp_vydej.base_class_label = "Typ výdeje léčivého přípravku";
config.owl_headers.dlp_vydej.base_class_comment = "Typ výdeje léčivého přípravku";

//dlp_zavislost
config.rdf_uris.owl.dlp_zavislost = {};
config.rdf_uris.owl.dlp_zavislost.base = config.rdf_uris.owl.base + "dlp_zavislost/";
config.rdf_uris.owl.dlp_zavislost.datatype_property_zav = config.rdf_uris.owl.dlp_zavislost.base + "zav";
config.rdf_uris.owl.dlp_zavislost.datatype_property_nazev_cs = config.rdf_uris.owl.dlp_zavislost.base + "nazev_cs";

config.rdf_uris.rdf.dlp_zavislost = {};
config.rdf_uris.rdf.dlp_zavislost.base = config.rdf_uris.rdf.base + "dlp_zavislost/";

config.owl_headers.dlp_zavislost = {};
config.owl_headers.dlp_zavislost.ontology_title = "Závislost";
config.owl_headers.dlp_zavislost.ontology_description = "Závislost";
config.owl_headers.dlp_zavislost.base_class_label = "Závislost";
config.owl_headers.dlp_zavislost.base_class_comment = "Závislost";

//dlp_zdroje
config.rdf_uris.owl.dlp_zdroje = {};
config.rdf_uris.owl.dlp_zdroje.base = config.rdf_uris.owl.base + "dlp_zdroje/";
config.rdf_uris.owl.dlp_zdroje.datatype_property_zdroj = config.rdf_uris.owl.dlp_zdroje.base + "zdroj";
config.rdf_uris.owl.dlp_zdroje.datatype_property_nazev = config.rdf_uris.owl.dlp_zdroje.base + "nazev";

config.rdf_uris.rdf.dlp_zdroje = {};
config.rdf_uris.rdf.dlp_zdroje.base = config.rdf_uris.rdf.base + "dlp_zdroje/";

config.owl_headers.dlp_zdroje = {};
config.owl_headers.dlp_zdroje.ontology_title = "Zdroje";
config.owl_headers.dlp_zdroje.ontology_description = "Zdroje";
config.owl_headers.dlp_zdroje.base_class_label = "Zdroje";
config.owl_headers.dlp_zdroje.base_class_comment = "Zdroje";

//dlp_zeme
config.rdf_uris.owl.dlp_zeme = {};
config.rdf_uris.owl.dlp_zeme.base = config.rdf_uris.owl.base + "dlp_zeme/";
config.rdf_uris.owl.dlp_zeme.datatype_property_zem = config.rdf_uris.owl.dlp_zeme.base + "zem";
config.rdf_uris.owl.dlp_zeme.datatype_property_nazev = config.rdf_uris.owl.dlp_zeme.base + "nazev";
config.rdf_uris.owl.dlp_zeme.datatype_property_nazev_en = config.rdf_uris.owl.dlp_zeme.base + "nazev_en";

config.rdf_uris.rdf.dlp_zeme = {};
config.rdf_uris.rdf.dlp_zeme.base = config.rdf_uris.rdf.base + "dlp_zeme/";

config.owl_headers.dlp_zeme = {};
config.owl_headers.dlp_zeme.ontology_title = "Země";
config.owl_headers.dlp_zeme.ontology_description = "Země";
config.owl_headers.dlp_zeme.base_class_label = "Země";
config.owl_headers.dlp_zeme.base_class_comment = "Země";


//dlp_lecivepripravky
config.rdf_uris.owl.dlp_lecivepripravky = {};
config.rdf_uris.owl.dlp_lecivepripravky.base = config.rdf_uris.owl.base + "dlp_lecivepripravky/";
config.rdf_uris.owl.dlp_lecivepripravky.cena = "http://www.mzcr.cz/leky.aspx?lek=";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_kod_sukl = config.rdf_uris.owl.dlp_lecivepripravky.base + "kod_sukl";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_h = config.rdf_uris.owl.dlp_lecivepripravky.base + "h";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_nazev = config.rdf_uris.owl.dlp_lecivepripravky.base + "nazev";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_doplnek = config.rdf_uris.owl.dlp_lecivepripravky.base + "doplnek";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_cesta = config.rdf_uris.owl.dlp_lecivepripravky.base + "cesta";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_forma = config.rdf_uris.owl.dlp_lecivepripravky.base + "forma";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_baleni = config.rdf_uris.owl.dlp_lecivepripravky.base + "baleni";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_sila = config.rdf_uris.owl.dlp_lecivepripravky.base + "sila";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_obal = config.rdf_uris.owl.dlp_lecivepripravky.base + "obal";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_drz = config.rdf_uris.owl.dlp_lecivepripravky.base + "drz";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_zemdr = config.rdf_uris.owl.dlp_lecivepripravky.base + "zemdr";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_akt_drz = config.rdf_uris.owl.dlp_lecivepripravky.base + "akt_drz";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_akt_zem = config.rdf_uris.owl.dlp_lecivepripravky.base + "akt_zem";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_reg= config.rdf_uris.owl.dlp_lecivepripravky.base + "reg";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_v_platdo = config.rdf_uris.owl.dlp_lecivepripravky.base + "v_platdo";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_neomez = config.rdf_uris.owl.dlp_lecivepripravky.base + "neomez";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_uvadenido = config.rdf_uris.owl.dlp_lecivepripravky.base + "uvadenido";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_is_ = config.rdf_uris.owl.dlp_lecivepripravky.base + "is_";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_atc_who = config.rdf_uris.owl.dlp_lecivepripravky.base + "atc_who";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_rc = config.rdf_uris.owl.dlp_lecivepripravky.base + "rc";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_sdov = config.rdf_uris.owl.dlp_lecivepripravky.base + "sdov";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_sdov_dov = config.rdf_uris.owl.dlp_lecivepripravky.base + "sdov_dov";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_sdov_zem = config.rdf_uris.owl.dlp_lecivepripravky.base + "sdov_zem";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_reg_proc = config.rdf_uris.owl.dlp_lecivepripravky.base + "reg_proc";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_dddamnt_who = config.rdf_uris.owl.dlp_lecivepripravky.base + "dddamnt_who";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_dddun_who = config.rdf_uris.owl.dlp_lecivepripravky.base + "dddun_who";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_ddd = config.rdf_uris.owl.dlp_lecivepripravky.base + "ddd";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_zdroj_who = config.rdf_uris.owl.dlp_lecivepripravky.base + "zdroj_who";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_ll = config.rdf_uris.owl.dlp_lecivepripravky.base + "ll";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_vydej = config.rdf_uris.owl.dlp_lecivepripravky.base + "vydej";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_zav = config.rdf_uris.owl.dlp_lecivepripravky.base + "zav";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_doping = config.rdf_uris.owl.dlp_lecivepripravky.base + "doping";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_narvla = config.rdf_uris.owl.dlp_lecivepripravky.base + "narvla";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_dodavky = config.rdf_uris.owl.dlp_lecivepripravky.base + "dodavky";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_ean = config.rdf_uris.owl.dlp_lecivepripravky.base + "ean";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_braillovo_pismo = config.rdf_uris.owl.dlp_lecivepripravky.base + "braillovo_pismo";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_exp = config.rdf_uris.owl.dlp_lecivepripravky.base + "exp";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_exp_t = config.rdf_uris.owl.dlp_lecivepripravky.base + "exp_t";
config.rdf_uris.owl.dlp_lecivepripravky.datatype_property_cena = config.rdf_uris.owl.dlp_lecivepripravky.base + "cena";

config.rdf_uris.rdf.dlp_lecivepripravky = {};
config.rdf_uris.rdf.dlp_lecivepripravky.base = config.rdf_uris.rdf.base + "dlp_lecivepripravky/";
config.rdf_uris.rdf.dlp_lecivepripravky.cena = "http://www.mzcr.cz/leky.aspx?lek=";

config.owl_headers.dlp_lecivepripravky = {};
config.owl_headers.dlp_lecivepripravky.ontology_title = "Léčivé přípravky";
config.owl_headers.dlp_lecivepripravky.ontology_description = "Léčivé přípravky";
config.owl_headers.dlp_lecivepripravky.base_class_label = "Léčivé přípravky";
config.owl_headers.dlp_lecivepripravky.base_class_comment = "Léčivé přípravky";



//dlp_latky
config.rdf_uris.owl.dlp_latky = {};
config.rdf_uris.owl.dlp_latky.base = config.rdf_uris.owl.base + "dlp_latky/";
config.rdf_uris.owl.dlp_latky.datatype_property_kod_latky = config.rdf_uris.owl.dlp_latky.base + "kod_latky";
config.rdf_uris.owl.dlp_latky.datatype_property_zdroj = config.rdf_uris.owl.dlp_latky.base + "zdroj";
config.rdf_uris.owl.dlp_latky.datatype_property_nazev_inn = config.rdf_uris.owl.dlp_latky.base + "nazev_inn";
config.rdf_uris.owl.dlp_latky.datatype_property_nazev_en = config.rdf_uris.owl.dlp_latky.base + "nazev_en";
config.rdf_uris.owl.dlp_latky.datatype_property_nazev = config.rdf_uris.owl.dlp_latky.base + "nazev";
config.rdf_uris.owl.dlp_latky.datatype_property_zav = config.rdf_uris.owl.dlp_latky.base + "zav";
config.rdf_uris.owl.dlp_latky.datatype_property_dop = config.rdf_uris.owl.dlp_latky.base + "dop";
config.rdf_uris.owl.dlp_latky.datatype_property_narvla = config.rdf_uris.owl.dlp_latky.base + "narvla";


config.rdf_uris.rdf.dlp_latky = {};
config.rdf_uris.rdf.dlp_latky.base = config.rdf_uris.rdf.base + "dlp_latky/";

config.owl_headers.dlp_latky = {};
config.owl_headers.dlp_latky.ontology_title = "Látky";
config.owl_headers.dlp_latky.ontology_description = "Látky";
config.owl_headers.dlp_latky.base_class_label = "Látky";
config.owl_headers.dlp_latky.base_class_comment = "Látky";

//dlp_lecivelatky
config.rdf_uris.owl.dlp_lecivelatky = {};
config.rdf_uris.owl.dlp_lecivelatky.base = config.rdf_uris.owl.base + "dlp_lecivelatky/";
config.rdf_uris.owl.dlp_lecivelatky.datatype_property_kod_latky = config.rdf_uris.owl.dlp_lecivelatky.base + "kod_latky";
config.rdf_uris.owl.dlp_lecivelatky.datatype_property_nazev_inn = config.rdf_uris.owl.dlp_lecivelatky.base + "nazev_inn";
config.rdf_uris.owl.dlp_lecivelatky.datatype_property_nazev_en = config.rdf_uris.owl.dlp_lecivelatky.base + "nazev_en";
config.rdf_uris.owl.dlp_lecivelatky.datatype_property_nazev = config.rdf_uris.owl.dlp_lecivelatky.base + "nazev";

config.rdf_uris.rdf.dlp_lecivelatky = {};
config.rdf_uris.rdf.dlp_lecivelatky.base = config.rdf_uris.rdf.base + "dlp_lecivelatky/";

config.owl_headers.dlp_lecivelatky = {};
config.owl_headers.dlp_lecivelatky.ontology_title = "Léčivé látky";
config.owl_headers.dlp_lecivelatky.ontology_description = "Léčivé látky";
config.owl_headers.dlp_lecivelatky.base_class_label = "Léčivé látky";
config.owl_headers.dlp_lecivelatky.base_class_comment = "Léčivé látky";
//dlp_nazvydokumentu
config.rdf_uris.owl.dlp_nazvydokumentu = {};
config.rdf_uris.owl.dlp_nazvydokumentu.base = config.rdf_uris.owl.base + "dlp_nazvydokumentu/";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_kod_sukl = config.rdf_uris.owl.dlp_nazvydokumentu.base + "kod_sukl";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_pil = config.rdf_uris.owl.dlp_nazvydokumentu.base + "pil";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_dat_roz_pil = config.rdf_uris.owl.dlp_nazvydokumentu.base + "dat_roz_pil";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_spc = config.rdf_uris.owl.dlp_nazvydokumentu.base + "spc";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_dat_roz_spc = config.rdf_uris.owl.dlp_nazvydokumentu.base + "dat_roz_spc";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_obal_text = config.rdf_uris.owl.dlp_nazvydokumentu.base + "obal_text";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_dat_roz_obal = config.rdf_uris.owl.dlp_nazvydokumentu.base + "dat_roz_obal";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_nr = config.rdf_uris.owl.dlp_nazvydokumentu.base + "nr";
config.rdf_uris.owl.dlp_nazvydokumentu.datatype_property_dat_npm_nr = config.rdf_uris.owl.dlp_nazvydokumentu.base + "dat_npm_nr";

config.rdf_uris.rdf.dlp_nazvydokumentu = {};
config.rdf_uris.rdf.dlp_nazvydokumentu.base = config.rdf_uris.rdf.base + "dlp_nazvydokumentu/";

config.owl_headers.dlp_nazvydokumentu = {};
config.owl_headers.dlp_nazvydokumentu.ontology_title = "Názvy dokumentů";
config.owl_headers.dlp_nazvydokumentu.ontology_description = "Názvy dokumentů";
config.owl_headers.dlp_nazvydokumentu.base_class_label = "Názvy dokumentů";
config.owl_headers.dlp_nazvydokumentu.base_class_comment = "Názvy dokumentů";
//dlp_organizace
config.rdf_uris.owl.dlp_organizace = {};
config.rdf_uris.owl.dlp_organizace.base = config.rdf_uris.owl.base + "dlp_organizace/";
config.rdf_uris.owl.dlp_organizace.datatype_property_zkr_org = config.rdf_uris.owl.dlp_organizace.base + "zkr_org";
config.rdf_uris.owl.dlp_organizace.datatype_property_zem = config.rdf_uris.owl.dlp_organizace.base + "zem";
config.rdf_uris.owl.dlp_organizace.datatype_property_nazev = config.rdf_uris.owl.dlp_organizace.base + "nazev";
config.rdf_uris.owl.dlp_organizace.datatype_property_vyrobce = config.rdf_uris.owl.dlp_organizace.base + "vyrobce";
config.rdf_uris.owl.dlp_organizace.datatype_property_drzitel = config.rdf_uris.owl.dlp_organizace.base + "drzitel";



config.rdf_uris.rdf.dlp_organizace = {};
config.rdf_uris.rdf.dlp_organizace.base = config.rdf_uris.rdf.base + "dlp_organizace/";

config.owl_headers.dlp_organizace = {};
config.owl_headers.dlp_organizace.ontology_title = "Organizace";
config.owl_headers.dlp_organizace.ontology_description = "Organizace";
config.owl_headers.dlp_organizace.base_class_label = "Organizace";
config.owl_headers.dlp_organizace.base_class_comment = "Organizace";
//dlp_slozeni
config.rdf_uris.owl.dlp_slozeni = {};
config.rdf_uris.owl.dlp_slozeni.base = config.rdf_uris.owl.base + "dlp_slozeni/";
config.rdf_uris.owl.dlp_slozeni.datatype_property_kod_sukl = config.rdf_uris.owl.dlp_slozeni.base + "kod_sukl";
config.rdf_uris.owl.dlp_slozeni.datatype_property_kod_latky = config.rdf_uris.owl.dlp_slozeni.base + "kod_latky";
config.rdf_uris.owl.dlp_slozeni.datatype_property_sq = config.rdf_uris.owl.dlp_slozeni.base + "sq";
config.rdf_uris.owl.dlp_slozeni.datatype_property_s = config.rdf_uris.owl.dlp_slozeni.base + "s";
config.rdf_uris.owl.dlp_slozeni.datatype_property_amnt_od = config.rdf_uris.owl.dlp_slozeni.base + "amnt_od";
config.rdf_uris.owl.dlp_slozeni.datatype_property_amnt = config.rdf_uris.owl.dlp_slozeni.base + "amnt";
config.rdf_uris.owl.dlp_slozeni.datatype_property_un = config.rdf_uris.owl.dlp_slozeni.base + "un";


config.rdf_uris.rdf.dlp_slozeni = {};
config.rdf_uris.rdf.dlp_slozeni.base = config.rdf_uris.rdf.base + "dlp_slozeni/";

config.owl_headers.dlp_slozeni = {};
config.owl_headers.dlp_slozeni.ontology_title = "Složení";
config.owl_headers.dlp_slozeni.ontology_description = "Složení";
config.owl_headers.dlp_slozeni.base_class_label = "Složení";
config.owl_headers.dlp_slozeni.base_class_comment = "Složení";

// /dlp_slozenilist
config.rdf_uris.owl.dlp_slozenilist = {};
config.rdf_uris.owl.dlp_slozenilist.base = config.rdf_uris.owl.base + "dlp_slozenilist/";
config.rdf_uris.owl.dlp_slozenilist.datatype_property_kod_sukl = config.rdf_uris.owl.dlp_slozenilist.base + "kod_sukl";


config.rdf_uris.rdf.dlp_slozenilist = {};
config.rdf_uris.rdf.dlp_slozenilist.base = config.rdf_uris.rdf.base + "dlp_slozenilist/";

config.owl_headers.dlp_slozenilist = {};
config.owl_headers.dlp_slozenilist.ontology_title = "Složení daného přípravku";
config.owl_headers.dlp_slozenilist.ontology_description = "Složení daného přípravku";
config.owl_headers.dlp_slozenilist.base_class_label = "Složení daného přípravku";
config.owl_headers.dlp_slozenilist.base_class_comment = "Složení daného přípravku";



module.exports = config;


// /dlp_solilist
config.rdf_uris.owl.dlp_solilist = {};
config.rdf_uris.owl.dlp_solilist.base = config.rdf_uris.owl.base + "dlp_solilist/";
config.rdf_uris.owl.dlp_solilist.datatype_property_kod_latky = config.rdf_uris.owl.dlp_solilist.base + "kod_latky";


config.rdf_uris.rdf.dlp_solilist = {};
config.rdf_uris.rdf.dlp_solilist.base = config.rdf_uris.rdf.base + "dlp_solilist/";

config.owl_headers.dlp_solilist = {};
config.owl_headers.dlp_solilist.ontology_title = "Seznam solí látky";
config.owl_headers.dlp_solilist.ontology_description = "Seznam solí látky";
config.owl_headers.dlp_solilist.base_class_label = "Seznam solí látky";
config.owl_headers.dlp_solilist.base_class_comment = "Seznam solí látky";



module.exports = config;


// /dlp_soli
config.rdf_uris.owl.dlp_soli = {};
config.rdf_uris.owl.dlp_soli.base = config.rdf_uris.owl.base + "dlp_soli/";
config.rdf_uris.owl.dlp_soli.datatype_property_kod_latky = config.rdf_uris.owl.dlp_soli.base + "kod_latky";
config.rdf_uris.owl.dlp_soli.datatype_property_kod_soli = config.rdf_uris.owl.dlp_soli.base + "kod_soli";


config.rdf_uris.rdf.dlp_soli = {};
config.rdf_uris.rdf.dlp_soli.base = config.rdf_uris.rdf.base + "dlp_soli/";

config.owl_headers.dlp_soli = {};
config.owl_headers.dlp_soli.ontology_title = "Soli";
config.owl_headers.dlp_soli.ontology_description = "Soli";
config.owl_headers.dlp_soli.base_class_label = "Soli";
config.owl_headers.dlp_soli.base_class_comment = "Soli";

// /dlp_synonyma
config.rdf_uris.owl.dlp_synonyma = {};
config.rdf_uris.owl.dlp_synonyma.base = config.rdf_uris.owl.base + "dlp_synonyma/";
config.rdf_uris.owl.dlp_synonyma.datatype_property_kod_latky = config.rdf_uris.owl.dlp_synonyma.base + "kod_latky";
config.rdf_uris.owl.dlp_synonyma.datatype_property_sq = config.rdf_uris.owl.dlp_synonyma.base + "sq";
config.rdf_uris.owl.dlp_synonyma.datatype_property_ = config.rdf_uris.owl.dlp_synonyma.base + "zdroj";
config.rdf_uris.owl.dlp_synonyma.datatype_property_ = config.rdf_uris.owl.dlp_synonyma.base + "nazev";

config.rdf_uris.rdf.dlp_synonyma = {};
config.rdf_uris.rdf.dlp_synonyma.base = config.rdf_uris.rdf.base + "dlp_synonyma/";

config.owl_headers.dlp_synonyma = {};
config.owl_headers.dlp_synonyma.ontology_title = "SynonymA";
config.owl_headers.dlp_synonyma.ontology_description = "Synonyma";
config.owl_headers.dlp_synonyma.base_class_label = "Synonyma";
config.owl_headers.dlp_synonyma.base_class_comment = "Synonyma";



// /dlp_vpois
config.rdf_uris.owl.dlp_vpois = {};
config.rdf_uris.owl.dlp_vpois.base = config.rdf_uris.owl.base + "dlp_vpois/";
config.rdf_uris.owl.dlp_vpois.datatype_property_kod_sukl = config.rdf_uris.owl.dlp_vpois.base + "kod_sukl";
config.rdf_uris.owl.dlp_vpois.datatype_property_vpois_nazev_spolecnosti = config.rdf_uris.owl.dlp_vpois.base + "vpois_nazev_spolecnosti";
config.rdf_uris.owl.dlp_vpois.datatype_property_vpois_www = config.rdf_uris.owl.dlp_vpois.base + "vpois_www";
config.rdf_uris.owl.dlp_vpois.datatype_property_vpois_email = config.rdf_uris.owl.dlp_vpois.base + "vpois_email";
config.rdf_uris.owl.dlp_vpois.datatype_property_vpois_tel = config.rdf_uris.owl.dlp_vpois.base + "vpois_tel";


config.rdf_uris.rdf.dlp_vpois = {};
config.rdf_uris.rdf.dlp_vpois.base = config.rdf_uris.rdf.base + "dlp_vpois/";

config.owl_headers.dlp_vpois = {};
config.owl_headers.dlp_vpois.ontology_title = "Informační servis držitele";
config.owl_headers.dlp_vpois.ontology_description = "Informační servis držitele";
config.owl_headers.dlp_vpois.base_class_label = "Informační servis držitele";
config.owl_headers.dlp_vpois.base_class_comment = "Informační servis držitele";


module.exports = config;

// /dlp_synonyma
config.rdf_uris.owl.dlp_vyrobci = {};
config.rdf_uris.owl.dlp_vyrobci.base = config.rdf_uris.owl.base + "dlp_vyrobci/";
config.rdf_uris.owl.dlp_vyrobci.datatype_property_kod_sukl = config.rdf_uris.owl.dlp_vyrobci.base + "kod_sukl";
config.rdf_uris.owl.dlp_vyrobci.datatype_property_vyr = config.rdf_uris.owl.dlp_vyrobci.base + "vyr";
config.rdf_uris.owl.dlp_vyrobci.datatype_property_zem = config.rdf_uris.owl.dlp_vyrobci.base + "zem";



config.rdf_uris.rdf.dlp_vyrobci = {};
config.rdf_uris.rdf.dlp_vyrobci.base = config.rdf_uris.rdf.base + "dlp_vyrobci/";

config.owl_headers.dlp_vyrobci = {};
config.owl_headers.dlp_vyrobci.ontology_title = "Informační servis držitele";
config.owl_headers.dlp_vyrobci.ontology_description = "Informační servis držitele";
config.owl_headers.dlp_vyrobci.base_class_label = "Informační servis držitele";
config.owl_headers.dlp_vyrobci.base_class_comment = "Informační servis držitele";


// rxclass
config.rdf_uris.owl.rxclass = {};
config.rdf_uris.owl.rxclass.base = config.rdf_uris.owl.base + "rxclass/";
config.rdf_uris.owl.rxclass.datatype_property_minConceptRxcui = config.rdf_uris.owl.rxclass.base + "minConceptRxcui";
config.rdf_uris.owl.rxclass.datatype_property_minConceptName = config.rdf_uris.owl.rxclass.base + "minConceptName";
config.rdf_uris.owl.rxclass.datatype_property_minConceptTty = config.rdf_uris.owl.rxclass.base + "minConceptTty";
config.rdf_uris.owl.rxclass.datatype_property_sourceId = config.rdf_uris.owl.rxclass.base + "sourceId";
config.rdf_uris.owl.rxclass.datatype_property_sourceName = config.rdf_uris.owl.rxclass.base + "sourceName";
config.rdf_uris.owl.rxclass.datatype_property_relation = config.rdf_uris.owl.rxclass.base + "relation";




config.rdf_uris.rdf.rxclass = {};
config.rdf_uris.rdf.rxclass.base = config.rdf_uris.rdf.base + "rxclass/";

config.owl_headers.rxclass = {};
config.owl_headers.rxclass.ontology_title = "Rxclass";
config.owl_headers.rxclass.ontology_description = "Rxclass";
config.owl_headers.rxclass.base_class_label = "Rxclass";
config.owl_headers.rxclass.base_class_comment = "Rxclass";





module.exports = config;
