import axios from "axios";
import cookie from "react-cookie";

export const LOGIN_LOGIN = "LOGIN";
export const LOGIN_AUTHORIZE = "LOGIN_AUTHORIZE";
export const LOGIN_SAVE_ACCESS_TOKEN = "LOGIN_SAVE_ACCESS_TOKEN";
export const LOGIN_LOAD_ACCESS_TOKEN = "LOGIN_LOAD_ACCESS_TOKEN";
export const LOGIN_CLEAR_ACCESS_TOKEN = "LOGIN_CLEAR_ACCESS_TOKEN";
export const LOGIN_AUTHORIZE_PENDING = "LOGIN_AUTHORIZE_PENDING";

const ROOT_URL = "http://localhost:3000/users";
const COOKIE_ACCESSTOKEN = "access_token"

/**
 * Odeslání informací z formuláře na server
 *
 * @param values
 */
export function login(username, password) {

    const request = axios.post(`${ROOT_URL}/login`, {username, password});

    return {
        type: LOGIN_LOGIN,
        payload: request,

    }
}

export  function authorizePending(boolean) {
    return{
        type: LOGIN_AUTHORIZE_PENDING,
        payload: boolean
    }
}

/**
 * reautorizuje se přes access token
 * @param acessToken
 * @returns {{type: string, payload: *}}
 */
export function authorize(accessToken){
    const request = axios.post(`${ROOT_URL}/authorize`, {accessToken: accessToken});

    return {
        type: LOGIN_AUTHORIZE,
        payload: request
    }
}

/**
 * uloží access token
 *
 * @param accessToken
 * @returns {{type: string, payload: *}}
 */
export function saveAccessToken(accessToken) {
    cookie.save(COOKIE_ACCESSTOKEN, accessToken, { path: '/' });
    return {
        type: LOGIN_SAVE_ACCESS_TOKEN,
        payload: accessToken
    };
}

/**
 * Načte access token z cookie
 */
export function loadAccessToken(){
    return {
        type: LOGIN_LOAD_ACCESS_TOKEN,
        payload: cookie.load(COOKIE_ACCESSTOKEN)
    };
}

export function logout(){
    cookie.remove(COOKIE_ACCESSTOKEN, { path: '/' });
    return clearAccessToken();
}
/**
 * Smažte cookie - obvykle po odhlášení
 */
export function clearAccessToken(){
    cookie.remove(COOKIE_ACCESSTOKEN, { path: '/' });
    return {
        type: LOGIN_CLEAR_ACCESS_TOKEN,
    };
}

