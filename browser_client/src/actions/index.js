import axios from "axios";
import _ from "lodash";

//akce pro změnu jazyka
export const CHANGE_LANG = "CHANGE_LANG";
export const FETCH_DRUGS_BY_NAME = "FETCH_DRUGS_BY_NAME";
export const FETCH_DRUG_BY_ID = "FETCH_DRUG_BY_ID";
export const FETCH_DRUG_COUNT_NAME = "FETCH_DRUG_COUNT_NAME";
export const CHANGE_QUERY_WORD = "CHANGE_QUERY_WORD";
export const CLEAR_DETAIL = "CLEAR_DETAIL";
export const FETCH_PRICES_DRUG_BY_ID = "FETCH_PRICE_DRUG_BY_ID";
export const FETCH_DRUG_FORMULATION_BY_DRUG_ID = "FETCH_DRUG_FORMULATION_BY_DRUG_ID";
export const FETCH_ALL_SIMILAR_DRUG_BY_ATC = "FETCH_ALL_SIMILAR_DRUG_BY_ATC";
export const CHANGE_PAGE_SIMILAR_DRUGS = "CHANGE_PAGE_SIMILAR_DRUGS"
export const FETCH_COUNT_ALL_SIMILAR_DRUG_BY_ATC = "FETCH_COUNT_ALL_SIMILAR_DRUG_BY_ATC"
export const FETCH_INTERACTION_BY_ID= "FETCH_INTERACTION_BY_ID"

const ROOT_URL = "http://localhost:3000/";

export const PAGINATION_INDEX = 10;

export function changeLang(lang) {
    return {
        type: CHANGE_LANG,
        payload: lang
    }
}
/**
 * Akce pro získání všech léků na základě jména
 *
 * @param name
 * @returns {{type: string, payload: *}}
 */
export function fetchDrugsByName(name, page) {
    var name = name || "";
    var page = page || 0;
    var limit = PAGINATION_INDEX;
    var offset = page * PAGINATION_INDEX;

    const request = axios.get(`${ROOT_URL}dlp_lecivepripravky/findAllByNamePattern?name=${name}&limit=${limit}&offset=${offset}`);
    return {
        type: FETCH_DRUGS_BY_NAME,
        payload: request
    };
}

export function countByName(name) {
    var name = name || "";
    const request = axios.get(`${ROOT_URL}dlp_lecivepripravky/countByNamePattern?name=${name}`);

    return {
        type: FETCH_DRUG_COUNT_NAME,
        payload: request
    };
}
/**
 * Akce pro získání léku na základě id
 *
 * @param name
 * @returns {{type: string, payload: *}}
 */
export function fetchDrugById(id) {
    const request = axios.get(`${ROOT_URL}dlp_lecivepripravky/findById?id=${id}`);


    return {
        type: FETCH_DRUG_BY_ID,
        payload: request
    };
}

export function fetchPricesById(id) {
    const request = axios.get(`${ROOT_URL}prices/findById?id=${id}`);
    return{
        type: FETCH_PRICES_DRUG_BY_ID,
        payload:request
    }
}

export function fetchDrugFormulationByDrugId(id) {
    const request = axios.get(`${ROOT_URL}dlp_lecivepripravky/findDrugFormulationByDrugId?id=${id}`);

    return{
        type: FETCH_DRUG_FORMULATION_BY_DRUG_ID,
        payload:request
    }
}
/**
 * vrací list podobných léků
 *
 * @param id - kod sukl
 * @returns {{type: string, payload: *}}
 */
export function fetchAllSimilarDrugByATC(id,page){
    var page = page || 0;
    var limit = PAGINATION_INDEX;
    var offset = page * PAGINATION_INDEX;
    const request = axios.get(`${ROOT_URL}dlp_lecivepripravky/findAllSimilarDrugByATC?id=${id}&limit=${limit}&offset=${offset}`);

    return{
        type: FETCH_ALL_SIMILAR_DRUG_BY_ATC,
        payload:request
    }

}

export function fetchCountAllSimilarDrugByATC(id){
    var name = name || "";
    const request = axios.get(`${ROOT_URL}dlp_lecivepripravky/countAllSimilarDrugByATC?id=${id}`);

    return {
        type: FETCH_COUNT_ALL_SIMILAR_DRUG_BY_ATC,
        payload: request
    };

}


export function fetchInteractionById(id){
    const request = axios.get(`${ROOT_URL}rxnav/findInteractionById?id=${id}`);

    return {
        type: FETCH_INTERACTION_BY_ID,
        payload: request
    };

}

/**
 * Pro změnu slova při vyhledávání léků
 *
 * @param query
 * @returns {{type: string, payload: *}}
 */
export function changeQueryWord(query) {
    return {
        type: CHANGE_QUERY_WORD,
        payload: query
    };
}

export function changePageSimilarDrugs(page) {
    return {
        type: CHANGE_PAGE_SIMILAR_DRUGS,
        payload: page


    };
}

/**
 * Resetuje původní detail
 *
 * @returns {{type: string, payload: null}}
 */
export function clearDetail(){
    return {
        type:CLEAR_DETAIL,
        payload: null
    };
}

