/**
 * Created by radims on 06.02.16.
 */
var request  = require("request");
var xml2js  = require("xml2js");
var async = require("async");

var template_system = require("../../modules/template-system/template-system");
var rxclass_mapper = require("../rxnav/rxclass_mapper");

const TEMPLATE_PATH_RDF = "rxclass/rdf.html";
const TEMPLATE_PATH_DEFAULT = "rxclass/default.html";

const RXNAV_URL ="https://rxnav.nlm.nih.gov/REST/rxclass/classMembers?relaSource=ATC&classId=";
const BASIC_ATC_CLASS = ["A","B","C","D","G","H","J","L","M","N","P","R","S","V"]
/**
 * Generate owl file of dlp_doping
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters
 */
module.exports.defaultAction = function(params, config,callback) {
    callback = callback || console.log;
    var template_path = config.base_paths.app.app_folder.presenters.templates.base + TEMPLATE_PATH_DEFAULT;
    var output_template_path = config.base_paths.app.temp.data.rxnav + "rxclass.owl";
    var template_vars = {
        owl_uris: config.rdf_uris.owl,
        rdf_uris: config.rdf_uris.rdf,
        headers: config.owl_headers
    };

    var template_system_object = {
        template: template_vars
    };
    template_system.render(template_path, output_template_path, template_system_object, callback);

};


/**
 * Generate rdf file of dlp_doping
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters
 */
module.exports.rdfAction = function(params, config,callback) {
    callback = callback || console.log;

    var array = []

    var template_path = config.base_paths.app.app_folder.presenters.templates.base + TEMPLATE_PATH_RDF;
    var output_template_path = config.base_paths.app.temp.data.rxnav + "rxclass.rdf";
    var template_vars = {
        owl_uris: config.rdf_uris.owl,
        rdf_uris: config.rdf_uris.rdf,
        headers: config.owl_headers
    };

    //for all main atc classes
    async.each(BASIC_ATC_CLASS,
        function(atcClass, callback){
            // do request to api
            request(RXNAV_URL + atcClass, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    //convert xml to json
                    xml2js.parseString(body,function(err,result) {
                        //map  object and add to array
                        result.rxclassdata.drugMemberGroup[0].drugMember.map(function(item){
                            array.push(rxclass_mapper.create(item));
                        })
                        callback();
                    })
                }
            })
        },
        function(err){
            var template_system_object = {
                mapper_output: array,
                template: template_vars
            };
            template_system.render(template_path, output_template_path, template_system_object, callback);

        }
    );

};
