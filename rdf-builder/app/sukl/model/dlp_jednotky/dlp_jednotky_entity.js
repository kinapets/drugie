/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_jednotky {
    constructor(un, nazev){
        if (jsutils.isSomeEmpty([un, nazev])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.un = un;
        this.nazev = nazev;
    }

}

module.exports = Dlp_jednotky;