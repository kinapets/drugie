/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import translations from "../../services/translator/translator";
import {Panel,Pagination} from "react-bootstrap";
import Loader from "../common/loader";
import {Link} from "react-router";
import {ROUTE_DETAILDRUG} from "../../routes";

import {connect} from "react-redux";

class SimilarTable extends Component {

    onPageClick(event, selectedEvent) {
        const page = selectedEvent.eventKey;

        this.props.changePage(page -1);
        this.props.fetchData(this.props.drug.kod_sukl.value,page -1 );
    }

    render() {

        const ACTIVE_LANG = this.props.langs.active;
        const drug_list = this.props.drug_list;

        if(!drug_list)
            return (
                <Loader/>
            );

        return (
            <Panel collapsible  header={`${translations.similar_table.table_title[ACTIVE_LANG]}`}>
                <table className="table">
                    <tbody>
                    {this.renderDrugList()}
                    </tbody>
                </table>
                <Pagination
                    prev
                    next
                    first
                    last
                    ellipsis
                    boundaryLinks
                    items={this.props.itemCount}
                    maxButtons={5}
                    activePage={this.props.currentPage+1}
                    onSelect={this.onPageClick.bind(this)}/>
            </Panel>
        );
    }



    //Renderuje seznam léků
    renderDrugList() {

        return this.props.drug_list.map((function (item) {
            return (
                <tr key={item.kod_sukl.value}>
                    <td>
                        <Link to={`${ROUTE_DETAILDRUG}/${item.kod_sukl.value}`}>
                            {item.kod_sukl.value}
                        </Link>
                    </td>
                    <td>{item.nazev.value}</td>
                    <td>{item.sila.value}</td>
                    <td>{item.doplnek.value}</td>
                    <td ><span className="pull-right">
                        <Link className="btn btn-default"   to={`${ROUTE_DETAILDRUG}/${item.kod_sukl.value}`}>
                            <i className="fa fa-list"></i>
                        </Link></span>
                    </td>
                </tr>
            );
        }).bind(this))
    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        langs: state.langs
    };
}

export default connect(mapStateToProps, null)(SimilarTable);
