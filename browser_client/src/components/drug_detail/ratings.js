/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import {Panel} from "react-bootstrap";
import translations from "../../services/translator/translator";
import {connect} from "react-redux";
import {fetchRatingsById} from "../../actions/ratings_actions";

import Stars from "./ratings/stars";
import RatingForm from "./ratings/rating_form";
import Loader from "../common/loader";

class Ratings extends Component {
    componentDidUpdate() {
        const ratings = this.props.ratings.ratings;
        const drug_detail = this.props.dlp_lecivepripravky.detail_drug;
        if (ratings == null && drug_detail) {
            this.props.fetchRatingsById(drug_detail[0].kod_sukl.value);
        }
    }

    render() {
        const ACTIVE_LANG = this.props.langs.active;
        const ratings = this.props.ratings.ratings;

        if(ratings){
            return (
                <Panel collapsible defaultExpanded header={`${translations.ratings.panel_title[ACTIVE_LANG]}`}>
                    <ul className="list-group">
                        {this.renderComments()}
                    </ul>
                    <RatingForm/>
                </Panel>
            );
        }else{
            return (
                <Panel collapsible defaultExpanded header={`${translations.ratings.panel_title[ACTIVE_LANG]}`}>
                    <ul className="list-group">
                        <Loader/>
                    </ul>
                    <RatingForm/>
                </Panel>
            );
        }




    }

    renderComments() {
        const ratings = this.props.ratings.ratings;
        if (ratings)
            return ratings.map(function (item) {
                return (
                    <li className="list-group-item" key={item.id}>
                        <h2>
                            <small>{item.username}</small>


                        </h2>
                        <Stars stars={item.rating}/>
                        <p>{item.comment}</p>
                    </li>
                )
            });
    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        langs: state.langs,
        ratings: state.ratings,
        dlp_lecivepripravky: state.dlp_lecivepripravky
    };
}

const actions = {fetchRatingsById};

export default connect(mapStateToProps, actions)(Ratings);

