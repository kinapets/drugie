/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';


export default class Stars extends Component {
    render() {
        const stars = this.props.stars;
        return (
            <div >
                {this.renderStars(stars)}
            </div>
        );



    }

    renderStars(count){
        var rows = [];
        for(var i = 0; i < count ; i++) {
            rows.push(<span className="pull-right color-blue"  key={i}><i className="fa fa-star" ></i></span>)
        }
        return rows;
    }
}

