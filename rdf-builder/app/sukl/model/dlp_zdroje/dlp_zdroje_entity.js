
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_zdroje {
    constructor(zdroj, nazev){
        if (jsutils.isSomeEmpty([zdroj, nazev])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.zdroj = zdroj;
        this.nazev = nazev;

    }

}

module.exports = Dlp_zdroje;