var fs = require("fs");
var Mustache = require("mustache")



module.exports.FIND_ALL_BY_NAME_PATTERN = "./sparql/findAllByNamePattern.sparql";
module.exports.FIND_BY_ID  = "./sparql/findById.sparql";
module.exports.COUNT_BY_NAME_PATTERN  = "./sparql/countByNamePattern.sparql";
module.exports.FIND_DRUG_FORMULATION_BY_DRUG_ID  = "./sparql/findDrugFormulationByDrugId.sparql";
module.exports.FIND_ALL_SIMILAR_DRUG_BY_ATC  = "./sparql/findAllSimilarDrugByATC.sparql";
module.exports.COUNT_ALL_SIMILAR_DRUG_BY_ATC = "./sparql/countAllSimilarDrugByATC.sparql";


/**
* Načte se template daného dotazu a vyplní se proměnné
*
* @param path - je to cesta k danému template dotazu
* @param variables - objekt popisující proměné, které potřebuje daný dotaz
*/
module.exports.load = function(path,variables){
  var query = fs.readFileSync(path).toString();
  var output = Mustache.render(query, variables)

  return output
}
