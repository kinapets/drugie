/**
 * Created by radims on 03.04.16.
 */
/* GET home page. */
var express = require('express');
var router = express.Router();
var request = require("request");
var stardog_api = require("../stardog_api/stardog_api");

var API_RXNAV= "https://rxnav.nlm.nih.gov/REST/interaction/interaction.json?";

/* GET home page. */
router.get('/findInteractionById', function(req, res, next) {
    var id = req.query.id || "";
    stardog_api.dlp_lecivepripravky().findById(id, function(data){
        const drug = data[0];
        if(!drug || !drug.rxcui)
            return res.json({interactionTypeGroup:[]});

        requestRxNav(drug.rxcui.value,function(body){
            const json = JSON.parse(body);
            res.json(json);
        })
    });
});

function requestRxNav(rxcui,callback){
    request(`${API_RXNAV}&rxcui=${rxcui}`, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            callback(body);
        }
    })

}

module.exports = router;