/**
 * Created by radims on 11.04.16.
 */
/**
 * Created by radims on 11.04.16.
 */
import axios from "axios";

export const RATINGFORM_CHANGE_STARS_COUNT = "RATINGFORM_CHANGE_STARS_COUNT";
export const RATINGFORM_CLEAR = "RATINGFORM_CLEAR";
export const RATINGFORM_SUBMIT = "RATINGFORM_SUBMIT";
export const RATINGFORM_ISRATINGADDED = "RATINGFORM_ISRATINGADDED";
export const RATINGFORM_ADD_RATING_LOCAL = "RATINGFORM_ADD_RATING_LOCAL";
export const RATINGFORM_IS_RATING_ADDED_PENDING= "RATINGFORM_IS_RATING_ADDED_PENDING";


const ROOT_URL = "http://localhost:3000/ratings";

export  function changeStarsCoutn(count){
    return {
        type: RATINGFORM_CHANGE_STARS_COUNT,
        payload: count
    };
}

export function addRatingLocally(username,comment, rating){
    return {
        type: RATINGFORM_ADD_RATING_LOCAL,
        payload: [{
            id: Date.now(),
            username: username,
            rating: rating,
            comment: comment
        }]
    };

}

export function isRatingAddedPending(boolean) {
    return {
        type: RATINGFORM_IS_RATING_ADDED_PENDING,
        payload: boolean
    };
}

export function isRatingAdded(kod_sukl, access_token) {
    const request = axios.get(`${ROOT_URL}/isAdded?kod_sukl=${kod_sukl}&access_token=${access_token}`);
    console.log(`${ROOT_URL}/isAdded?kod_sukl=${kod_sukl}&access_token=${access_token}`)
    return {
        type: RATINGFORM_ISRATINGADDED,
        payload: request
    };
}

export function submitForm(fields,kod_sukl,access_token) {
    const request = axios.post(`${ROOT_URL}/add?access_token=${access_token}&kod_sukl=${kod_sukl}`, fields);

    return {
        type: RATINGFORM_SUBMIT,
        payload: request
    }
}

export function clear() {

    return {
        type: RATINGFORM_CLEAR
    };
}







