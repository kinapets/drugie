var Rxclass = require("./rxclass_entity");

//xml attrName tag values - depends on
const ATTR_SOURCEID = "SourceId";
const ATTR_SOURCENAME = "SourceName";
const ATTR_RELATION = "Relation";

module.exports.create = function (object) {
    var rxclass = new Rxclass();
    var minConcept = object.minConcept[0];

    rxclass.setMinConceptRxcui(minConcept.rxcui[0])
    rxclass.setMinConceptName(minConcept.name[0])
    rxclass.setMinConceptTty(minConcept.tty[0])

    object.nodeAttr.map(function (item) {
        switch (item.attrName[0]) {
            case ATTR_SOURCEID:
                rxclass.setSourceId(item.attrValue[0]);
                break;
            case ATTR_SOURCENAME:
                rxclass.setSourceName(item.attrValue[0]);
                break;
            case ATTR_RELATION:
                rxclass.setRelation(item.attrValue[0]);
                break;
        }
    });
    return rxclass;
};