/**
 * Created by radims on 19.02.16.
 */

class Dlp_slozenibalnknode_entity {

    constructor(kod_sukl)
    {
        //Kód léčivého přípravku (dále LP) přidělený SÚKL variantě LP v rámci rozhodnutí o registraci LP, přidělený
        // neregistrovanému LP zařazenému do specifického léčebného programu (dále SLP) nebo přidělený potravině pro
        // zvláštní lékařské účely (dále PZLÚ)
        console.log(kod_sukl);
        this.kod_sukl = kod_sukl;
    }
}

module.exports = Dlp_slozenibalnknode_entity;