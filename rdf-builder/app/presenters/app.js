/**
 * Created by radims on 29.01.16.
 */

/**
 * This function decide which controller - action will be executed
 *
 * @param globalConfig - global variable to share application vars
 * @param presenterName - name of controller  we want to be executed
 * @param actionName - name of action in controller
 * @param params - some extra arguments for controller
 */
var router = function (globalConfig,presenterName,actionName,params) {
    //set default action if is necessary
    actionName = actionName || "default";
    if(!presenterName){
        console.log("First parameter is empty");
        return;
    }
    //create presenter action
    var presenterAction = createPresenterAction(presenterName,actionName)
    //execute presenter action
    try{
        presenterAction(params,globalConfig)
    }catch (e){
        throw new Error(presenterAction(params,globalConfig));
    };
};

module.exports = router;

/**
 * returns function of controller if exists
 * @param presenter
 * @param actionName
 * @returns {Function}
 */
function createPresenterAction(presenterName,actionName) {
    var presenter = createPresenter(presenterName);
    if (!presenter) return null;
    try{
        return eval("presenter."+actionName+"Action");
    }catch (e){
        throw new Error("This route does't exist " + e);
    }

}


/**
 * Cretates controller
 *
 * @param presenterName
 * @returns {*}
 */
function createPresenter(presenterName) {
    try{
        var presenter = require("./" + presenterName + "_presenter");
    }catch (e){
        throw e;

    }
    return presenter;

}



