
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_synonyma {
    constructor(kod_latky, sq, zdroj, nazev){
        if (jsutils.isSomeEmpty([kod_latky, sq, zdroj, nazev])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.kod_latky = kod_latky;
        this.sq = sq;
        this.zdroj = zdroj;
        this.nazev = nazev;

    }

}

module.exports = Dlp_synonyma;