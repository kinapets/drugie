/**
 * Created by radims on 27.03.16.
 */
import React from "react";
import {Route, IndexRoute} from "react-router";

import App from "./components/app";
import Home from "./components/home";
import Contact from "./components/contact";
import Search from "./components/search";
import MyDrugstore from "./components/my_drugstore";
import DrugDetail from "./components/drug_detail";
import Help from "./components/help";
import Page404 from "./components/page404";
import Login from "./components/login";
import Registration from "./components/registration";


export const ROUTE_INDEX = "/";
export const ROUTE_CONTACT = "/contact";
export const ROUTE_HELP = "/help";
export const ROUTE_SEARCH = "/search";
export const ROUTE_MYDRUGSTORE = "/drugstore";
export const ROUTE_DETAILDRUG = "/drug";
export const ROUTE_REGISTRATION = "/registration";
export const ROUTE_LOGIN = "/login";
export const ROUTE_PAGE404 = "*";


export default(
    <Route path="/" component={App}>
        <IndexRoute component={Home}/>
        <Route component={Contact} path={ROUTE_CONTACT}/>
        <Route component={Help} path={ROUTE_HELP}/>
        <Route component={Search} path={ROUTE_SEARCH}/>
        <Route component={MyDrugstore} path={ROUTE_MYDRUGSTORE}/>
        <Route component={DrugDetail} path={ROUTE_DETAILDRUG+ "/:id"}/>
        <Route component={Registration} path={ROUTE_REGISTRATION}/>
        <Route component={Login} path={ROUTE_LOGIN}/>
        <Route component={Page404} path={ROUTE_PAGE404}/>
    </Route>
);