/**
 * Created by radims on 06.02.16.
 */
var Entity = require("./dlp_lecivelatky_entity");


module.exports.create = function(line) {
    var arr = line.split(";");
    var entity = new Entity(arr[0], arr[1],arr[2],arr[3]);
    return entity;
};