/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import translations from "../../services/translator/translator";
import {Panel} from "react-bootstrap";
import Loader from "../common/loader";

import {connect} from "react-redux";

class PricesTable extends Component {
    render() {
        const ACTIVE_LANG = this.props.langs.active;

        if(this.props.prices === null)
            return (
                <Panel collapsible header={`${translations.prices_table.table_title[ACTIVE_LANG]}`}>
                <Loader/>
                </Panel>
            );

        return (
            <Panel collapsible header={`${translations.prices_table.table_title[ACTIVE_LANG]}`}>
                <table className="table">
                    <tbody>
                    {this.renderPrices()}
                    </tbody>
                </table>
            </Panel>
        );
    }

    renderPrices(){
        const ACTIVE_LANG = this.props.langs.active;

        if(this.props.prices.length > 0)
            return this.props.prices.map((function(item){
                return <tr key={item.title}>
                    <th>{this.translatePriceTitle(item)}</th>
                    <td>{item.value}</td>
                </tr>
            }).bind(this))
        else
            return (<tr><td>{translations.prices_table.no_prices[ACTIVE_LANG]}</td></tr>);
    }

    /**
     * Pokud je jako aktivní jazyk nastavena čeština potom se vypíše title jinak se to pokusí přeloži
     *
     * @param title
     * @returns {*}
     */
    translatePriceTitle(item){
        const ACTIVE_LANG = this.props.langs.active;
        try{
            return translations.prices_table[item.titleHash][ACTIVE_LANG];
        }catch (err){
            return `${item.title} (${translations.prices_table.no_translation[ACTIVE_LANG]})`;
        }

    }


}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        dlp_lecivepripravky: state.dlp_lecivepripravky,
        langs: state.langs
    };
}

export default connect(mapStateToProps, null)(PricesTable);