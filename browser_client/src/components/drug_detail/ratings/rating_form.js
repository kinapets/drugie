/**
 * Created by radims on 11.04.16.
 */
/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import translations from "../../../services/translator/translator";
import {reduxForm} from "redux-form";
import {
    changeStarsCoutn,
    clear as clearAction,
    addRatingLocally,
    isRatingAdded,
    isRatingAddedPending,
    submitForm
} from "./../../../actions/ratingform_actions";
import Authorized  from "../../common/authorized";
import Loader from "../../common/loader";



class RatingForm extends Component {
    componentDidMount() {
        this.props.clearAction();
    }

    componentDidUpdate(){
        const drug_detail = this.props.dlp_lecivepripravky.detail_drug;
        const loginReducer = this.props.loginReducer;
        const ratinfForm = this.props.ratingform;
        if(drug_detail && loginReducer.accessToken && !ratinfForm.isAddedPending){
            this.props.isRatingAddedPending(true);
            this.props.isRatingAdded(drug_detail[0].kod_sukl.value,loginReducer.accessToken);
        }
    }

    onMouseOver(starCount) {
        const {fields:{rating,comment}, handleSubmit} = this.props
        this.props.changeStarsCoutn(starCount);
        rating.onChange(starCount);
    }

    onSubmit(props) {
        const formFields = this.props.fields;
        const username = this.props.loginReducer.user.username;
        const accessToken = this.props.loginReducer.accessToken;
        const drug_detail = this.props.dlp_lecivepripravky.detail_drug[0];

        this.props.addRatingLocally(username, formFields.comment.value, formFields.rating.value)
        this.props.submitForm(formFields,drug_detail.kod_sukl.value,accessToken)
    }

    render() {
        const ratingForm = this.props.ratingform;
        const ACTIVE_LANG = this.props.langs.active;
        if(!ratingForm.isAdded)
            return this.renderForm();
        else
            return (<span><i> {translations.rating_form.rating_was_added[ACTIVE_LANG]}</i></span>);
    }

    renderForm(){
        const star_count = this.props.ratingform.starsCount;
        const ACTIVE_LANG = this.props.langs.active;
        const {fields:{rating,comment}, handleSubmit} = this.props;
        const drug_detail = this.props.dlp_lecivepripravky.detail_drug;
        const loginReducer = this.props.loginReducer;
        const ratinfForm = this.props.ratingform;

        //pokud jeste neni vsechno nactene tak se vykresli loader
        if(drug_detail && loginReducer.accessToken && !ratinfForm.isAddedPending){
            return (<Loader/>);
        }

        return (
            <Authorized authorize={true}>
                <div >
                    <i onMouseOver={()=>{this.onMouseOver(1)}}
                       className={`fa fa-star ${star_count >= 1 ? "color-red": ""}`}></i>
                    <i onMouseOver={()=>{this.onMouseOver(2)}}
                       className={`fa fa-star ${star_count >= 2 ? "color-red": ""}`}></i>
                    <i onMouseOver={()=>{this.onMouseOver(3)}}
                       className={`fa fa-star ${star_count >= 3 ? "color-red": ""}`}></i>
                    <i onMouseOver={()=>{this.onMouseOver(4)}}
                       className={`fa fa-star ${star_count >= 4 ? "color-red": ""}`}></i>
                    <i onMouseOver={()=>{this.onMouseOver(5)}}
                       className={`fa fa-star ${star_count >= 5 ? "color-red": ""}`}></i>
                    <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                        <div className="form-group">
                            <input type="hidden" value={star_count} onChange={()=>{alert("Test")}}
                                   className="form-control" {...rating}/>
                        </div>
                        <div className="form-group">
                            <textarea className="form-control"
                                      placeholder={translations.rating_form.text_area_placeholder[ACTIVE_LANG]} {...comment}></textarea>
                            <div className="control-label">
                                {
                                    comment.touched ? comment.error : ""
                                }
                                {
                                    comment.touched ? rating.error : ""
                                }
                            </div>
                        </div>
                        <button className="btn btn-primary"
                                type="submit">
                            {translations.rating_form.submit_title[ACTIVE_LANG]}
                        </button>
                    </form>
                </div>
            </Authorized>
        );
    }
}

/**
 * Validace formuláře - synchronní
 * @param values
 * @returns {*}
 */
function validate(values) {
    const errors = {};
    if (!values.rating || values.rating == 0) {
        errors.rating = "Enter some rating";
    }
    if (!values.comment) {
        errors.comment = "Enter some comment";
    }

    return errors;
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        langs: state.langs,
        ratingform: state.ratingform,
        loginReducer: state.login,
        dlp_lecivepripravky: state.dlp_lecivepripravky
    };
}

const actions = {
    changeStarsCoutn,
    clearAction,
    addRatingLocally,
    isRatingAdded,
    isRatingAddedPending,
    submitForm
}

export default reduxForm({
    form: "RatingForm", //unique token
    fields: ["rating", "comment"],
    validate,

}, mapStateToProps, actions)(RatingForm);

