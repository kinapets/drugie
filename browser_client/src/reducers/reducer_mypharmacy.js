/**
 * Created by radims on 27.03.16.
 */
import {
    MYPHARMACY_ADD,
    MYPHARMACY_LIST,
    MYPHARMACY_REMOVE
} from "../actions/mypharmacy_actions";

import {
    LOGIN_CLEAR_ACCESS_TOKEN
} from "../actions/login_actions";

const INITIAL_STATE = {
    drug_list: [], //všechny léky v mojí lékárně
    fetched: false

};


export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case MYPHARMACY_ADD:
        case MYPHARMACY_REMOVE:
        case MYPHARMACY_LIST:
            return {
                ...state,
                drug_list: action.payload.data,
                fetched: true
            }
        case LOGIN_CLEAR_ACCESS_TOKEN:
            return INITIAL_STATE
        default:
            return state;
    }
};
