/**
 * Created by radims on 07.04.16.
 */

import React from 'react';
import { Component } from 'react';

export default class InputFormGroup extends Component {
    render() {
        const field = this.props.field;
        const title = this.props.title;
        const type = this.props.type || "text";

        return (
            <div className={`form-group ${field.touched && field.invalid ? "has-error" : ""}`}>
                <label>{title}<span className="color-red">*</span></label>
                <input type={type} className="form-control margin-bottom-20"  {...field}/>
                <div className="control-label" >
                    {
                        field.touched ? field.error : ""
                    }
                </div>
            </div>
        );
    }
}

