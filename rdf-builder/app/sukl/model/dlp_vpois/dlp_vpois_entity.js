
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_vpois {
    constructor(kod_sukl,vpois_nazev_spolecnosti,vpois_www,vpois_email,vpois_tel){
        if (jsutils.isSomeEmpty([kod_sukl,vpois_nazev_spolecnosti,vpois_www,vpois_email,vpois_tel])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.kod_sukl = kod_sukl;
        this.vpois_nazev_spolecnosti = vpois_nazev_spolecnosti;
        this.vpois_www = vpois_www;
        this.vpois_email = vpois_email;
        this.vpois_tel = vpois_tel

    }

}

module.exports = Dlp_vpois;