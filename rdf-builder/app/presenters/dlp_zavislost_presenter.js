/**
 * Created by radims on 06.02.16.
 */


var mapper = require("../sukl/model/dlp_zavislost/dlp_zavislost_mapper");
var owlrdf_presenter = require("./_base_rdfowl_presenter");


const TEMPLATE_PATH_DEFAULT = "dlp_zavislost/default.html";
const TEMPLATE_PATH_RDF = "dlp_zavislost/rdf.html";

/**
 * Generate owl file of dlp_doping
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters
 */
module.exports.defaultAction = function(params, config,callback) {
    callback = callback || console.log;
    owlrdf_presenter.defaultAction(
        params,
        config,
        TEMPLATE_PATH_DEFAULT,
        config.file_names.sukl.dlp_zavislost,
        mapper,
        ".owl",
        callback
    );
};


/**
 * Generate rdf file of dlp_doping
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters
 */
module.exports.rdfAction = function(params, config,callback) {
    callback = callback || console.log;
    owlrdf_presenter.defaultAction(
        params,
        config,
        TEMPLATE_PATH_RDF,
        config.file_names.sukl.dlp_zavislost,
        mapper,
        ".rdf",
        callback
    );
};


