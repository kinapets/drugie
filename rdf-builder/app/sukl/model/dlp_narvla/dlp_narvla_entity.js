
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_narvla {
    constructor(narvla, nazev){
        if (jsutils.isSomeEmpty([narvla, nazev])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.narvla = narvla;
        this.nazev = nazev;
    }

}

module.exports = Dlp_narvla;