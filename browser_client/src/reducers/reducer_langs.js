/**
 * Created by radims on 27.03.16.
 */
import { CHANGE_LANG } from "../actions/index";

const INITIAL_STATE = {
    active: "cz",
    langs: ["cz","en"]

};

export default function(state = INITIAL_STATE, action) {
    switch (action.type){
        case CHANGE_LANG:
            return {... state, active:action.payload}
        default:
            return state;
    }
};