
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_organizace {
    constructor(zkr_org,zem,nazev,vyrobce,drzitel){
        if (jsutils.isSomeEmpty([zkr_org,zem,nazev,vyrobce,drzitel])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.zkr_org = zkr_org;
        this.zem = zem;
        this.nazev = nazev;
        this.vyrobce = vyrobce;
        this.drzitel = drzitel;
    }

}

module.exports = Dlp_organizace;