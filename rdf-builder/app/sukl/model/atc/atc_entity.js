;
/**
 * Class represents atc structure.
 */


class Atc{
    /**
     * creates atc
     * @param atc
     * @param nt
     * @param name
     * @param name_en
     */
    constructor(atc,nt,name,name_en){
        this.atc = atc;
        this.nt = nt;
        this.name = name;
        this.name_en = name_en;
        this.atc_structure = Atc.getAtcStructure(atc);
        this.atc_type = this.getAtcType();
        this.parent = this.atc_structure[this.atc_type - 1];

        if(!Atc.isAtcExist(this.parent)) {
            this.parent = this.atc_structure[this.atc_type - 2];
        }
    }


    /**
     * There is some problem in ATC structure. Some classes doesn t exist.
     * Function control if atc is exist or not.
     * @param atc
     * @returns {boolean}
     */
    static isAtcExist(atc){
        const NOT_ATC = [
            "Q06C","Q06A","Q01X","Q10C","Q10B","Q02C","Q02B","Q14A","Q01K","Q01J",
            "Q01H","Q15A","Q07B","Q15G","Q11A","Q03C","Q15D","Q03B","Q15C","Q15B",
            "Q04A","Q08E","Q16B","Q16A","Q08A","Q04I","Q04H","Q20A","Q12D","Q12C",
            "Q12B","Q04D","Q12A","Q04C","Q08G","Q04B","Q08F","Q05A","Q09C","Q09B",
            "Q09A","Q05I","Q01E","Q05G","Q01C","Q01B","Q01A", "Q05D","Q05C", "Q12J",
            "Q12I", "Q12H", "Q12G", "Q04X"
        ];

        return NOT_ATC.indexOf(atc) === -1 ? true : false;
    }


    /**
     * function returns array of atc strucure (parents), of specific atc
     * @param atc_string
     * @returns {Array}
     */
    static getAtcStructure(atc_string){
        var strlen = atc_string.length;
        var allowLength = [1, 3, 4, 5, 7];
        if(allowLength.indexOf(strlen) === -1) {
            throw new Error("Wrong format of ATC");
        }
        var parents = [];
        parents[0] = atc_string.charAt(0);
        parents[1] = atc_string.substr(1, 2) ? parents[0] + atc_string.substr(1, 2) : null;
        parents[2] = atc_string.charAt(3) ?parents[1] +atc_string.charAt(3) : null ;
        parents[3] = atc_string.charAt(4)?parents[2] +atc_string.charAt(4): null;
        parents[4] = atc_string.substr(5) ? parents[3] + atc_string.substr(5):null;

        return parents;
    }

    /**
     * returns int. depends on type of atc, there are 5 types of ATC
     * example:
     * A            return 0
     * A01          return 1
     * A01A         return 2
     * A01AA        return 3
     * A01AA01      return 4
     */
    getAtcType(){
        var length = this.atc.length;
        switch (length) {
            case 1:
                return 0;
            case 3:
                return 1;
            case 4:
                return 2;
            case 5:
                return 3;
            case 7:
                return 4;

        }
    }


    setAtc(atc){
        this.atc = atc;
    }

    setNt(nt){
        this.nt = nt;
    }

    setName(name){
        this.name = name;
    }

    setName_en(name_en){
        this.name_en = name_en;
    }

    setParent(parent){
        if(Atc.isAtcExist(parent)) {
            this.parent = parent;
        }else {
            throw new Error("This atc does not exist.")
        }
    }


    getAtc(){
        return this.atc;
    }

    getNt(){
        return this.nt;
    }
    getName(){
        return this.name;
    }

    getName_en(){
        return this.name_en;
    }

    getParent(){
        return this.parent;
    }

}

module.exports = Atc;