/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component,PropTypes } from 'react';
import Menu from "./common/menu";
import Loader from "./common/loader";
import Breadcrumb from "./common/breadcrumb";
import {connect} from "react-redux";
import Authorized from "./common/authorized";

import {Panel,Col,Label,Navbar, Input, Button} from "react-bootstrap";

import {
    fetchDrugById,
    clearDetail,
    fetchPricesById,
    fetchDrugFormulationByDrugId,
    fetchAllSimilarDrugByATC,
    changePageSimilarDrugs,
    fetchCountAllSimilarDrugByATC,
    fetchInteractionById,
    PAGINATION_INDEX
} from "../actions/index";


import {ROUTE_SEARCH} from "../routes";

import DrugTable from "./drug_detail/drug_table";
import PricesTable from "./drug_detail/prices_table";
import FilesTable from "./drug_detail/files_table";
import DrugFormulationTable from "./drug_detail/drug_formulation_table";
import SimilarTable from "./drug_detail/similar_table";
import DrugInteractionTable from "./drug_detail/drug_interaction_table";
import AddToPharmacyButton from "./drug_detail/add_to_pharmacy_button"
import Ratings from "./drug_detail/ratings";

import translations from "../services/translator/translator";

class DrugDetail extends Component {
    //naserovani abychom to meli v this.context.router
    static contextTypes = {
        router: PropTypes.object
    };

    componentWillMount() {
        this.fetchData(this.props.params.id);
    }

    /**
     * Toto vše kvůli kdyz v browseru macknu na tlacitko zpet - nerefetchoval se stav
     */
    componentDidUpdate() {
        const DETAIL_DRUG = this.props.dlp_lecivepripravky.detail_drug;
        //pokud už je nastavený stav
        if (DETAIL_DRUG) {
            //a zaroven se lisi stav a parametry v url
            if (DETAIL_DRUG[0].kod_sukl.value != this.props.params.id) {
                //refetchnu nový stav
                this.fetchData(this.props.params.id);
            }
        }
    }


    fetchData(id) {
        this.props.clearDetail();
        this.props.fetchDrugById(id);
        this.props.fetchPricesById(id);
        this.props.fetchDrugFormulationByDrugId(id);
        this.props.fetchAllSimilarDrugByATC(id)
        this.props.fetchCountAllSimilarDrugByATC(id);
        this.props.fetchInteractionById(id);
    }

    render() {
        if (!this.props.dlp_lecivepripravky.detail_drug)
            return this.renderLoading();
        else
            return this.renderDetail();
    }

    renderHeader() {
        const ACTIVE_LANG = this.props.langs.active;

        return (
            <div><Menu/>
                <Breadcrumb
                    title={`${translations.drug_detail.breadcrumb_title[ACTIVE_LANG]}`}
                    pages={[{linkTo:"/", title: "Drugie"},{linkTo:ROUTE_SEARCH,title:translations.search.breadcrumb_page[ACTIVE_LANG]}]}
                    last={translations.drug_detail.breadcrumb_page[ACTIVE_LANG]}
                /></div>
        );
    }

    renderLoading() {

        return (
            <div>
                {this.renderHeader()}
                <Col lg={12}>
                    <Loader/>
                </Col>
            </div>
        );
    }

    renderDetail() {
        const drug = this.props.dlp_lecivepripravky.detail_drug[0];
        const prices = this.props.dlp_lecivepripravky.detail_drug_prices;
        const drug_formulation = this.props.dlp_lecivepripravky.drug_formulation;
        const detail_drug_similar = this.props.dlp_lecivepripravky.detail_drug_similar;
        const detail_drug_similar_count = this.props.dlp_lecivepripravky.detail_drug_similar_count;
        return (
            <div>
                {this.renderHeader()}
                <div className="container">
                    <h1>{drug.nazev.value}
                        <small>{drug.kod_sukl.value}</small>
                        <Authorized authorize={true}><AddToPharmacyButton/></Authorized>
                    </h1>

                    <DrugTable
                        kod_sukl={drug.kod_sukl.value}
                        sila={drug.sila.value}
                        brailovo_pismo={drug.brailovo_pismo.value}
                        atc_name={drug.atc_name.value}
                        atc_name_en={drug.atc_name_en.value}
                        atc_who={drug.atc_who.value}/>
                    <FilesTable drug={drug}/>
                    <DrugFormulationTable drug_formulation={drug_formulation}/>
                    <PricesTable prices={prices}/>
                    <SimilarTable
                        drug={drug}
                        drug_list={detail_drug_similar}
                        fetchData={this.props.fetchAllSimilarDrugByATC.bind(this)}
                        changePage={this.props.changePageSimilarDrugs.bind(this)}
                        currentPage={this.props.dlp_lecivepripravky.detail_drug_similar_page}
                        itemCount={this.getItemCount(this.props.dlp_lecivepripravky.detail_drug_similar_count,PAGINATION_INDEX)}
                    />
                    <DrugInteractionTable
                        interaction={this.props.dlp_lecivepripravky.detail_drug_interaction_list}
                    />
                    <Ratings/>


                </div>
            </div>
        );


    }

    getItemCount(items, pagination_index) {
        return Math.ceil(items / pagination_index)
    }
}


//namapování stavu do props
function mapStateToProps(state) {
    return {
        dlp_lecivepripravky: state.dlp_lecivepripravky,
        langs: state.langs,
        loginReducer: state.login,
        myPharmacy: state.myPharmacy
    };
}

export default connect(mapStateToProps, {
    fetchDrugById,
    clearDetail,
    fetchPricesById,
    fetchDrugFormulationByDrugId,
    fetchAllSimilarDrugByATC,
    changePageSimilarDrugs,
    fetchCountAllSimilarDrugByATC,
    fetchInteractionById,
})(DrugDetail);