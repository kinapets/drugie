
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_stavyreg {
    constructor(reg, nazev){
        if (jsutils.isSomeEmpty([reg, nazev])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.reg = reg;
        this.nazev = nazev;

    }

}

module.exports = Dlp_stavyreg;