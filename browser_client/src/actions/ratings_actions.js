/**
 * Created by radims on 11.04.16.
 */
import axios from "axios";

export const RATINGS_FETCH = "RATINGS_FETCH";
export const RATINGS_CLEAR = "RATINGS_CLEAR";

const ROOT_URL = "http://localhost:3000/ratings";


/**
 * Získání hodncení
 *
 * @param values
 */
export function fetchRatingsById(kod_sukl) {

    const request = axios.get(`${ROOT_URL}/bySuklId?kod_sukl=${kod_sukl}`);

    return {
        type: RATINGS_FETCH,
        payload: request
    }
}

export function clearRatings() {
    return {
        type: RATINGS_CLEAR
    }
}



