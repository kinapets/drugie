/**
 * Created by radims on 04.03.16.
 */

/**
 * Created by radims on 29.02.16.
 */

var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_nazvydokumentu_entity{
    constructor(kod_sukl,pil,dat_roz_pil,spc,dat_roz_spc,obal_text,dat_roz_obal,nr,dat_npm_nr){
        if (jsutils.isSomeEmpty([kod_sukl,pil,dat_roz_pil,spc,dat_roz_spc,obal_text,dat_roz_obal,nr,dat_npm_nr])) {
            throw new Error("class dlp_cesty_entity: all variables in costructor are mandatory")
        }
        this.kod_sukl = kod_sukl.trim();
        this.pil = pil.trim();
        this.dat_roz_pil = dat_roz_pil.trim();
        this.spc = spc.trim();
        //this.spc_url = "";
        //if(this.spc.length> 20) {
        //    this.spc_url = this.spc;
        //    this.spc = "";
        //}
        //console.log(this.spc);
        //console.log(this.spc_url);
        this.dat_roz_spc = dat_roz_spc.trim();
        this.obal_text = obal_text.trim();
        this.dat_roz_obal = dat_roz_obal.trim();
        this.nr = nr.trim();
        this.dat_npm_nr = dat_npm_nr.trim();

    }

}

module.exports = Dlp_nazvydokumentu_entity;