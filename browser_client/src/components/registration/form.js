/**
 * Created by radims on 07.04.16.
 */
import React, {Component, PropTypes} from "react";
import {reduxForm} from "redux-form";
import {Link} from "react-router";
import InputFormGroup from "./input_form_group";
import {
    formSubmitted,
    serverSideValidate,
    validateForm,
    register,
    clearState
} from "../../actions/registration_actions";
import {ROUTE_LOGIN} from "../../routes";


class RegistrationForm extends Component {


    //naserovani abychom to meli v this.context.router
    static contextTypes = {
        router: PropTypes.object
    };

    componentWillMount() {
        this.props.clearState();
    }

    onSubmit(props) {
        const form = this.props;
        this.props.formSubmitted(false);
        this.props.register(form.fields)

    }

    componentDidUpdate() {
        const registration_succeded = this.props.registration.succeded;
        const form_submitted = this.props.registration.form_submitted;
        if (registration_succeded) {
            //tlacitko je stale disabled
            if (form_submitted)
                this.props.formSubmitted(false);
            //redirect probehne az po nejake dobe
            setTimeout((function () {
                this.context.router.push(ROUTE_LOGIN);
            }).bind(this,ROUTE_LOGIN), 1000);

        }
    }


    onSubmitClick(event) {
        const form = this.props;

        if (form.valid && !form.asyncValidating)
            this.props.formSubmitted(true);
    }

    onAlertDismisClick(event) {
        this.props.clearState();
    }

    renderErrorSubmitted() {
        const registration_succeded = this.props.registration.succeded;
        if (registration_succeded === false) {
            return (
                <div className="alert alert-danger fade in">
                    <button onClick={this.onAlertDismisClick.bind(this)} type="button" className="close"
                            data-dismiss="alert" aria-hidden="true">×
                    </button>
                    <h4>Oh snap! You got an error!</h4>
                    <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor
                        ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
                </div>
            );
            //else se nic nezobrazí protože není třeba zobrazovat
        }
    }


    renderSuccedSubmitted() {
        const registration_succeded = this.props.registration.succeded;
        if (registration_succeded === true) {
            return (
                <div className="alert alert-success fade in">
                    <button onClick={this.onAlertDismisClick.bind(this)} type="button" className="close"
                            data-dismiss="alert" aria-hidden="true">×
                    </button>
                    <h4>Oh snap! You got an error!</h4>
                    <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor
                        ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
                </div>
            );
            //else se nic nezobrazí protože není třeba zobrazovat
        }
    }

    render() {
        const {fields:{firstName,lastName, email, password, password2}, handleSubmit} = this.props
        const isFormSubmitted = this.props.registration.form_submitted;


        return (
            <form className="reg-page" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                <div className="reg-header">


                    <h2>Register a new account</h2>
                    <p>Already Signed Up? Click <a href="page_login.html" className="color-green">Sign In</a> to login
                        your account.
                    </p>
                </div>
                {this.renderErrorSubmitted()}
                {this.renderSuccedSubmitted()}

                <InputFormGroup field={firstName} title="Firstname"/>
                <InputFormGroup field={lastName} title="Lastname"/>
                <InputFormGroup field={email} title="Email address"/>

                <div className="row">
                    <div className="col-sm-6">
                        <InputFormGroup field={password} type="password" title="Password"/>
                    </div>
                    <div className="col-sm-6">
                        <InputFormGroup field={password2} type="password" title="Confirm password"/>
                    </div>
                </div>
                <hr/>
                <div className="row">
                    <div className="col-lg-6 text-left">
                        <button
                            onClick={this.onSubmitClick.bind(this)}
                            className={`btn ${isFormSubmitted ?  "disabled btn btn-danger" : ""}`}
                            type="submit"
                        >
                            Odeslat
                        </button>
                    </div>
                </div>
            </form>
        );
    }
}

/**
 * Validace formuláře - synchronní
 * @param values
 * @returns {*}
 */
function validate(values) {
    return validateForm(values);
}

/**
 * Asynchronní validace formuláře
 *
 * @param values
 * @returns {*}
 */
const asyncValidate = (values) => {
    return serverSideValidate(values);
};


//namapování stavu do props
function mapStateToProps(state) {
    return {
        registration: state.registration,
        langs: state.langs
    };
}

const actions = {formSubmitted, serverSideValidate, register, clearState}


export default reduxForm({
    form: "RegistrationForm", //unique token
    fields: ["firstName", "lastName", "email", "password", "password2"],
    validate,
    asyncValidate,
    asyncBlurFields: ['email'],

}, mapStateToProps, actions)(RegistrationForm);

