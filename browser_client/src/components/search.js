import React from 'react';
import { Component } from 'react';
import {connect} from "react-redux";
import {Link} from "react-router";

import _ from "lodash";

import Menu from "./common/menu";
import Breadcrumb from "./common/breadcrumb";
import Loader from "./common/loader";
import SearchInput from "./search/search_input";
import {Pager,Pagination,PageItem,Col,Navbar, Input, Button} from "react-bootstrap";

import translations from "../services/translator/translator";

import {fetchDrugsByName,changeQueryWord,countByName, PAGINATION_INDEX} from "../actions/index";

import {ROUTE_DETAILDRUG} from "../routes";


class Search extends Component {
    /**
     *
     */
    componentWillMount() {
        const word = this.props.dlp_lecivepripravky.search_list_query.search_word;

        if (!word) {
            this.props.changeQueryWord({search_word: "", page: 0});
            this.props.fetchDrugsByName("");
            this.props.countByName("");
        }
    }


    onChangeSearch(event) {
        this.props.changeQueryWord({search_word: event.target.value, page: 0});
        this.props.countByName(event.target.value);
        if(event.target.value.length > 2){
            this.props.fetchDrugsByName(event.target.value);

        }

    }

    onSearchClick() {
        const word = this.props.dlp_lecivepripravky.search_list_query.search_word;
        const page = this.props.dlp_lecivepripravky.search_list_query.page;

        this.props.changeQueryWord({search_word: word, page: page});
        this.props.fetchDrugsByName(word);
        this.props.countByName(word);
    }


    onPageClick(event, selectedEvent) {
        const page = selectedEvent.eventKey;
        var word = this.props.dlp_lecivepripravky.search_list_query.search_word;

        this.props.changeQueryWord({search_word: word, page: page - 1});
        this.props.fetchDrugsByName(word, page - 1);
        this.props.countByName(word);
    }


    render() {
        const ACTIVE_LANG = this.props.langs.active;
        return (
            <div>
                <Menu/>
                <Breadcrumb
                    title={translations.search.breadcrumb_title[ACTIVE_LANG]}
                    pages={[{linkTo:"/", title: "Drugie"}]}
                    last={translations.search.breadcrumb_title[ACTIVE_LANG]}
                />
                <SearchInput
                    title={translations.search.vyhledat[ACTIVE_LANG]}
                    placeholder={translations.search.si_placeholder[ACTIVE_LANG]}
                    onInputChange={this.onChangeSearch.bind(this)}
                    onButtonClick={this.onSearchClick.bind(this)}
                    inputValue={this.props.dlp_lecivepripravky.search_list_query.search_word}
                />

                <div className="container">
                    <div className="row">
                        <div className="col-lg-5">
                            <h1>{this.props.dlp_lecivepripravky.search_list_query.search_word}
                                <small> {translations.search.pocet_vysledku[ACTIVE_LANG]} {this.props.dlp_lecivepripravky.search_result_count}</small>
                            </h1>
                        </div>
                        <div className="col-lg-7">
                            <div className="pull-right">
                                <Pagination
                                    prev
                                    next
                                    first
                                    last
                                    ellipsis
                                    boundaryLinks
                                    items={((items,pagination_index)=>{
                                            return Math.ceil(items / pagination_index)
                                        })(this.props.dlp_lecivepripravky.search_result_count,PAGINATION_INDEX)}
                                    maxButtons={5}
                                    activePage={this.props.dlp_lecivepripravky.search_list_query.page +1}
                                    onSelect={this.onPageClick.bind(this)}/>
                            </div>
                        </div>
                    </div>
                    <table className="table" style={{minHeight:600 + "px"}}>
                        <tbody>
                        {this.renderDrugList()}
                        </tbody>
                    </table>


                </div>
            </div>

        );

    }

    //Renderuje seznam léků
    renderDrugList() {
        const search_list = this.props.dlp_lecivepripravky.search_list;
        const loading = this.props.dlp_lecivepripravky.loading;

        //když se nahrává
        if (loading)
            return (<tr>
                <td><Loader/></td>
            </tr>);

        //pokud není nic v listu
        if (search_list.length === 0) {
            return (<tr>
                <td>Nic tu není</td>
            </tr>);
        }

        return search_list.map(function (item) {
            return (
                <tr key={item.kod_sukl.value}>
                    <td>
                        <Link to={`${ROUTE_DETAILDRUG}/${item.kod_sukl.value}`}>
                            {item.kod_sukl.value}
                        </Link>
                    </td>
                    <td>{item.nazev.value}</td>
                    <td>{item.sila.value}</td>
                    <td>{item.doplnek.value}</td>
                    <td ><span className="pull-right">
                        <Link className="btn btn-default" to={`${ROUTE_DETAILDRUG}/${item.kod_sukl.value}`}>
                            <i className="fa fa-list"></i>
                        </Link></span>
                    </td>
                </tr>
            );
        })
    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        dlp_lecivepripravky: state.dlp_lecivepripravky,
        langs: state.langs
    };
}

export default connect(mapStateToProps, {fetchDrugsByName, changeQueryWord, countByName})(Search);