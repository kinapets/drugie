/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import {connect} from "react-redux";
import {login ,loadAccessToken,saveAccessToken,authorizePending,authorize as authorizeAction} from "../../actions/login_actions";


class Authorized extends Component {
    componentWillMount(){
        this.props.loadAccessToken();
    }

    componentDidUpdate() {
        const loginReducer = this.props.loginReducer
        //uživatel je přihlášen ale nebylo to uloženo do cookie zatím co aplikace běží (bez reloadu)
        if(loginReducer.loggedIn && !loginReducer.savedInCookie){
            this.props.saveAccessToken(loginReducer.accessToken);//persis daný access token
        }
        //pokud nebylo použito
        if(!loginReducer.savedInCookie && loginReducer.accessToken && !loginReducer.isAuthorizePending){
            this.props.authorizePending(true);
            this.props.authorizeAction(this.props.loginReducer.accessToken);
        }
    }

    render() {
        const loginReducer = this.props.loginReducer;
        const authorize = this.props.authorize;

        if(authorize){
            if(loginReducer.loggedIn){
                return this.props.children;
            }else{
                return <span></span>;
            }
        }else{
            if(!loginReducer.loggedIn){
                return this.props.children;
            }else{
                return <span></span>;
            }
        }

    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        loginReducer: state.login
    };
}

const actions = {login,loadAccessToken,saveAccessToken,authorizeAction,authorizePending}

export default connect(mapStateToProps, actions)(Authorized);
