/**
 * Created by radims on 27.03.16.
 */
export default {
    menu: {
        czech: {
            cz: "Česky",
            en: "Czech"
        },
        english: {
            cz: "Anglicky",
            en: "English"
        },
        login: {
            cz: "Přihlášení",
            en: "Login"
        },
        logout: {
            cz: "Odlhášení",
            en: "Login"
        },
        help: {
            cz: "Nápověda",
            en: "Help"
        },
        about: {
            cz: "O aplikaci",
            en: "About"
        },
        contact: {
            cz: "Kontakt",
            en: "Contact"
        },
        languages: {
            cz: "jazyky",
            en: "languages"
        },
        drug_search: {
            cz: "Vyhledávání",
            en: "Drug Search"
        },
        my_drugstore: {
            cz: "Moje lékárna",
            en: "My drugstore"
        },
        profil: {
            cz: "Profil",
            en: "Profil"
        },
        registration: {
            cz: "Registrace",
            en: "Registration"
        }

    },
    drug_detail: {
        basic_info: {
            cz: "Základní informace",
            en: "Basic info",
        },kod_sukl: {
            cz: "Kód SÚKL",
            en: "Code of sukl",
        },
        sila: {
            cz: "Síla léčivého přípravku",
            en: "Power",
        },
        brailovo_pismo: {
            cz: "Braillovo písmo",
            en: "Braille",
        },
        atc:{
            cz: "ATC",
            en: "ATC - czech name"
        },
        atc_name_en:{
            cz: "ATC - anglický název",
            en: "ATC - english name"
        },
        atc_who:{
            cz:"Kod ATC",
            en: "Code of ATC"
        },

        s: {
            cz: "Schváleno",
            en: "Approved",
        },
        v: {
            cz: "Výjimka",
            en: "Exception",
        },
        breadcrumb_title: {
            cz: "Detail",
            en: "Detail"
        },
        breadcrumb_page: {
            cz: "Detail",
            en: "Detail"
        }
    },
    search: {
        vyhledat: {
            cz: "Vyhledávání léčivých přípravků",
            en: "Search again"
        },
        si_placeholder: {
            cz: "Vyhledávání léčivých přípravku podle názvu",
            en: "Search by drug name"
        },
        pocet_vysledku: {
            cz: "počet výsledků",
            en: "number of results"
        },
        breadcrumb_title: {
            cz: "Vyhledávání",
            en: "Searching"
        },
        breadcrumb_page: {
            cz: "Výsledky vyhledávání",
            en: "Search results"
        }
    },
    prices_table:{
        table_title:{
            cz:"Informace o cenách",
            en: "Information about prices"
        },
        no_translation:{
            cz:"Překlad není dostupný",
            en: "Translation is not supported"
        },
        no_prices:{
            cz: "Ceny o léčivém přípravku nejsou dostupné",
            en: "Prices are not available."
        },
        379691444:{
            cz:"Maximální úhrada pojišťovny",
            en:"Maximal price for "
        },
        460719620:{
            cz:"Nejvyšší možná cena v lékárně při výdeji na recept",
            en:"The highest possible price at the pharmacy when dispensing a prescription"
        },
        1057533393:{
            cz:"Nejvyšší možný doplatek pacienta",
            en:"The highest possible supplement patient"
        },
        477217876:{
            cz:"Průměrná aktuální cena v lékárně",
            en:"The average current pharmacy price"
        },
        2115836868:{
            cz:"Průměrný aktuální doplatek pacienta",
            en:"Average actual supplement patient"
        },
        1411841024:{
            cz:"Výše doplatku započitatelného do ročního limitu",
            en:"Additional payment of offsetting the annual limit"
        }
    },
    files_table:{
        table_title:{
            cz:"Dokumenty",
            en: "Documents"
        },
        no_files:{
            cz:"Dokumenty nejsou dostupné",
            en:"There are no documents"

        },
        pil:{
            cz:"Příbalová informace",
            en: "PIL"
        },
        spc:{
            cz:"Souhrn údajů o přípravku",
            en:"SPC"
        },
        obal_text:{
            cz:"Text na obalu",
            en:"Text on box"
        }

    },
    drug_formulation_table:{
        table_title:{
            cz:"Složení",
            en: "Drug formulation"
        }
    },
    similar_table:{
        table_title:{
            cz:"Podobné léky",
            en: "Drug formulation"
        }
    },
    drug_interaction_table:{
        table_title:{
            cz: "Interakce mezi léky",
            en: "Interaction"
        },
        no_interactions:{
            cz: "Nebyly nalezeny žádné interakce mezi léčivy",
            en: "System did not search any interaction."
        }
    },
    registration:{
        breadcrumb_title: {
            cz: "Registrace",
            en: "Registration"
        },
        breadcrumb_page: {
            cz: "Registrace",
            en: "Registration"
        }

    },
    ratings:{
        panel_title: {
            cz: "Hodnocení",
            en: "Ratings"
        }

    },
    drugstore:{
        breadcrumb_title: {
            cz: "Moje lékárna",
            en: "My pharmacy"
        },
        breadcrumb_page: {
            cz: "Moje lékárna",
            en: "My pharmacy"
        },
        panel_title:{
            cz: "Moje lékárna",
            en: "My pharmacy"
        }

    },
    rating_form:{
        text_area_placeholder:{
            cz:"Komentář",
            en:"Comment"
        },
        submit_title:{
            cz:"Vložit",
            en: "Add"
        },
        rating_was_added:{
            cz: "Hodnocení již bylo přidáno",
            en: "Rating has been added yet"

        }
    },






};

