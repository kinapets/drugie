/**
 * Created by radims on 02.04.16.
 */
import React from 'react';
import { Component } from 'react';
import {Link} from "react-router";

export default class Breadcrumb extends Component {
    renderList() {
        return this.props.pages.map(function (item) {
            return (
                <li key={item.title}><Link to={item.linkTo}>{item.title}</Link></li>
            );
        });
    }

    render() {
        return (
            <div className="breadcrumbs-v3">
                <div className="container">
                    <h1 className="pull-left">{this.props.title}</h1>
                    <ul className="pull-right breadcrumb">
                        {this.renderList()}
                        <li className="active">{this.props.last}</li>
                    </ul>
                </div>
            </div>
        );
    }
}



