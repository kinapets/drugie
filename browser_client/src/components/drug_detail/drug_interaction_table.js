/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import _ from "lodash";
import { Component } from 'react';
import translations from "../../services/translator/translator";
import {Panel} from "react-bootstrap";
import Loader from "../common/loader";
import InteractionPairList from "./drug_interaction_table/interaction_pair_list";

import {connect} from "react-redux";

class DrugInteractionTable extends Component {

    render() {

        const ACTIVE_LANG = this.props.langs.active;

        if (this.props.interaction === null)
            return (
                <Panel collapsible
                       header={translations.drug_interaction_table.table_title[ACTIVE_LANG]}>
                <Loader/>
                </Panel>
            );
        if (_.isEmpty(this.props.interaction))
            return (
                <Panel collapsible
                       header={translations.drug_interaction_table.table_title[ACTIVE_LANG]}>
                    {translations.drug_interaction_table.no_interactions[ACTIVE_LANG]}
                </Panel>
            );


        return (
            <Panel collapsible header={translations.drug_interaction_table.table_title[ACTIVE_LANG]}>
                <table className="table">
                    <tbody>
                    {this.renderInteractions(this.props.interaction[0])}
                    </tbody>
                </table>
            </Panel>
        );
    }

    renderInteractions(interaction) {
        return interaction.interactionType.map(function (interactionType) {
            return (
                <tr key={interactionType.comment}>
                    <td>
                        <h2>{interactionType.comment}</h2>
                        <ul className="list-group">
                            <InteractionPairList interactionPairList={interactionType.interactionPair}/>
                        </ul>
                    </td>
                </tr>
            );

        });
    }


}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        dlp_lecivepripravky: state.dlp_lecivepripravky,
        langs: state.langs
    };
}

export default connect(mapStateToProps, null)(DrugInteractionTable);
/**
 * Created by radims on 03.04.16.
 */
