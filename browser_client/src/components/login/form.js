/**
 * Created by radims on 07.04.16.
 */

import React, {Component, PropTypes} from "react";
import {reduxForm} from "redux-form";
import {Link} from "react-router";
import InputGroup  from "./input_group";
import {login,loadAccessToken,saveAccessToken,authorize,logout} from "../../actions/login_actions";
import Authorized from "../common/authorized";
import {ROUTE_INDEX} from "../../routes";

class LoginForm extends Component {

    //naserovani abychom to meli v this.context.router
    static contextTypes = {
        router: PropTypes.object
    };

    componentDidUpdate(){
        if (this.props.loginReducer.loggedIn) {
            this.context.router.push(ROUTE_INDEX);
        }
    }

    onSubmit(props) {
        const formFields = this.props.fields;
        this.props.logout();
        this.props.login(formFields.username.value, formFields.password.value);
    }

    onAlertDismisClick(event) {
        this.props.logout();
    }

    renderErrorSubmitted() {

        if (this.props.loginReducer.status=== false) {
            return (
                <div className="alert alert-danger fade in">
                    <button onClick={this.onAlertDismisClick.bind(this)} type="button" className="close"
                            data-dismiss="alert" aria-hidden="true">×
                    </button>
                    <h4>Oh snap! You got an error!</h4>
                    <p>Change this and that and try again. Duis mollis, est non commodo luctus, nisi erat porttitor
                        ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum.</p>
                </div>
            );
            //else se nic nezobrazí protože není třeba zobrazovat
        }
    }

    render() {
        const {fields:{username,password}, handleSubmit} = this.props
        return (
            <Authorized authorize={false}>
                <form className="reg-page" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <div className="reg-header">
                        <h2>Login to your account </h2>
                    </div>
                    {this.renderErrorSubmitted()}
                    <InputGroup
                        icon="user"
                        field={username}
                        placeholder="Username"
                    />
                    <InputGroup
                        icon="lock"
                        field={password}
                        placeholder="Password"
                        type="password"
                    />
                    <div className="row">
                        <div className="col-md-6">
                            <button className="btn-u " type="submit">Login</button>
                        </div>
                    </div>
                    <hr/>
                    <h4>Have you been signed in?</h4>
                    <p>no worries, <a className="color-green" href="#">click here</a> to registration.</p>
                </form>
            </Authorized>
        );
    }
}

/**
 * Validace formuláře - synchronní
 * @param values
 * @returns {*}
 */
function validate(values) {
    const errors = {};
    if (!values.username) {
        errors.firstName = "Enter a firstName";
    }


    if (!values.password) {
        errors.password = "Enter some password";
    }
    return errors;
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        loginReducer: state.login,
        langs: state.langs
    };
}

const actions = {login, loadAccessToken, saveAccessToken, authorize,logout}

export default reduxForm({
    form: "LoginForm", //unique token
    fields: ["username", "password"],
    validate,

}, mapStateToProps, actions)(LoginForm);

