
/**
 * Created by radims on 03.04.16.
 */
/* GET home page. */
var express = require('express');
var router = express.Router();
var stardog_api = require("../stardog_api/stardog_api");


const mockDrug1 = {
    nazev: {type: 'literal', value: 'DEPAKINE'},
    exp: {type: 'literal', value: '60'},
    brailovo_pismo: {type: 'literal', value: 'V'},
    atc_who: {type: 'literal', value: 'N03AG01'},
    sila: {type: 'literal', value: '100MG/ML'},
    cena: {
        type: 'uri',
        value: 'http://www.mzcr.cz/leky.aspx?lek=151050'
    },
    kod_sukl: {type: 'literal', value: '151050'},
    atc_name: {type: 'literal', value: 'Kyselina valproová'},
    atc_name_en: {type: 'literal', value: 'valproic acid'},
    pil: {type: 'literal', value: 'PI82355.pdf'},
    spc: {type: 'literal', value: 'SPC82356.pdf'},
    obal_text: {type: 'literal', value: 'OBAL75997.pdf'},
    nr: {type: 'literal', value: ''},
    dat_npm_nr: {type: 'literal', value: ''},
    slozeni_list: {
        type: 'uri',
        value: 'http://db.cz/rdf/dlp_slozenilist/151050'
    },
    rxcui: {type: 'literal', value: '11118'},
    sourceId: {type: 'uri', value: 'http://db.cz/rdf/atc/N03AG01'},
    minConceptName: {type: 'literal', value: 'Valproic Acid'},
    sourceName: {type: 'literal', value: 'valproic acid'},
    relation: {type: 'literal', value: 'INDIRECT'}
};

const mockDrug2 = { nazev: { type: 'literal', value: 'DEPAKINE' },
    exp: { type: 'literal', value: '36' },
    brailovo_pismo: { type: 'literal', value: 'S' },
    atc_who: { type: 'literal', value: 'N03AG01' },
    sila: { type: 'literal', value: '50MG/ML' },
    cena: { type: 'uri', value: 'http://www.mzcr.cz/leky.aspx?lek=76378' },
    kod_sukl: { type: 'literal', value: '76378' },
    atc_name: { type: 'literal', value: 'Kyselina valproová' },
    atc_name_en: { type: 'literal', value: 'valproic acid' },
    pil: { type: 'literal', value: 'PI86925.pdf' },
    spc: { type: 'literal', value: 'SPC86926.pdf' },
    obal_text: { type: 'literal', value: 'OBAL86924.pdf' },
    nr: { type: 'literal', value: '' },
    dat_npm_nr: { type: 'literal', value: '' },
    slozeni_list: { type: 'uri', value: 'http://db.cz/rdf/dlp_slozenilist/76378' },
    rxcui: { type: 'literal', value: '11118' },
    sourceId: { type: 'uri', value: 'http://db.cz/rdf/atc/N03AG01' },
    minConceptName: { type: 'literal', value: 'Valproic Acid' },
    sourceName: { type: 'literal', value: 'valproic acid' },
    relation: { type: 'literal', value: 'INDIRECT' } };

/**
 * Dotáže na stránky ministerstva a stáhne a rozparsuje tabulkz
 */
router.post('/add', function(req, res, next) {
    const access_token = req.query.access_token;
    const drug = req.body;
    console.log(drug)
    res.json([
        mockDrug1,mockDrug2
    ]);
});

/**
 * Dotáže na stránky ministerstva a stáhne a rozparsuje tabulkz
 */
router.post('/remove', function(req, res, next) {
    const access_token = req.query.access_token;
    const drug = req.body;
    res.json([
        mockDrug1
    ]);
});


router.get("/list", function (req, res) {
    const access_token = req.query.access_token;

    res.json([
        mockDrug1,mockDrug2
    ]);

});

module.exports = router;