import axios from "axios";

export const MYPHARMACY_ADD = "MYPHARMACY_ADD";
export const MYPHARMACY_REMOVE = "MYPHARMACY_REMOVE";
export const MYPHARMACY_LIST = "MYPHARMACY_LIST";

const ROOT_URL = "http://localhost:3000/mypharmacy";


/**
 * Odeslání informací z formuláře na server
 *
 * @param values
 */
export function addToMyPharmacy(drug,access_token) {

    const request = axios.post(`${ROOT_URL}/add?access_token=${access_token}`, drug);

    return {
        type: MYPHARMACY_ADD,
        payload: request
    }
}

export function removeFromMyPharmacy(drug,access_token) {
    const request = axios.post(`${ROOT_URL}/remove?access_token=${access_token}`, drug);

    return {
        type: MYPHARMACY_REMOVE,
        payload: request
    }
}


/**
 * Odeslání informací z formuláře na server
 *
 * @param values
 */
export function fetchMyPharmacy(access_token) {

    const request = axios.get(`${ROOT_URL}/list?access_token=${access_token}`);

    return {
        type: MYPHARMACY_LIST,
        payload: request
    }
}

