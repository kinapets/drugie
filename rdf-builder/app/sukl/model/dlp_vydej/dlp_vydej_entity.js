
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_vydej {
    constructor(vydej, nazev){
        if (jsutils.isSomeEmpty([vydej, nazev])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.vydej = vydej;
        this.nazev = nazev;

    }

}

module.exports = Dlp_vydej;