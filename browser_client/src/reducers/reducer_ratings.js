/**
 * Created by radims on 07.04.16.
 */
import {
    RATINGS_FETCH,
    RATINGS_CLEAR
} from "../actions/ratings_actions";

import {
    RATINGFORM_ADD_RATING_LOCAL,
    RATINGFORM_CLEAR
} from "../actions/ratingform_actions";

const INITIAL_STATE = {
    ratings: null
};

export default function(state = INITIAL_STATE, action) {
    switch (action.type){
        case RATINGS_FETCH:
            return {
                ... state,
                ratings:action.payload.data
            }
        case RATINGS_CLEAR:
        case RATINGFORM_CLEAR:
            return INITIAL_STATE;
        case RATINGFORM_ADD_RATING_LOCAL:
            return {
                ...state,
                ratings:state.ratings.concat(action.payload)
            }

        default:
            return state;
    }
};
