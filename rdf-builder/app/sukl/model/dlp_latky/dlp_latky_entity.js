/**
 * Created by radims on 29.02.16.
 */

var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_latky_entity{
    constructor(kod_latky,zdroj,nazev_inn, nazev_en, nazev, zav, dop, narvla){
        if (jsutils.isSomeEmpty([kod_latky,zdroj,nazev_inn, nazev_en, nazev, zav, dop, narvla])) {
            throw new Error("class dlp_cesty_entity: all variables in costructor are mandatory")
        }

        this.kod_latky = kod_latky;
        this.zdroj = zdroj;
        this.nazev_inn = nazev_inn;
        this.nazev_en = nazev_en;
        this.nazev = nazev;
        this.zav = zav;
        this.dop = dop;
        this.narvla = narvla;


    }

}

module.exports = Dlp_latky_entity;