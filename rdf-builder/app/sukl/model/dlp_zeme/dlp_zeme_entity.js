
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_zeme {
    constructor(zem, nazev,nazev_en){
        if (jsutils.isSomeEmpty([zem, nazev,nazev_en])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.zem = zem;
        this.nazev = nazev;
        this.nazev_en = nazev_en;

    }

}

module.exports = Dlp_zeme;