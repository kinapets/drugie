/**
 * Created by radims on 11.04.16.
 */
/**
 * Created by radims on 07.04.16.
 */
import {
    RATINGFORM_CHANGE_STARS_COUNT,
    RATINGFORM_CLEAR,
    RATINGFORM_IS_RATING_ADDED_PENDING,
    RATINGFORM_ISRATINGADDED,
    RATINGFORM_ADD_RATING_LOCAL
} from "../actions/ratingform_actions";

const INITIAL_STATE = {
    starsCount: 0, //počet hvezdicek k odeslani
    isAdded: null, // jestli bylo
    isAddedPending: false, // is added pending bylo odeslano


};

export default function(state = INITIAL_STATE, action) {
    switch (action.type){
        case RATINGFORM_CHANGE_STARS_COUNT:
            return {
                ... state,
                starsCount:action.payload
            }
        case RATINGFORM_ADD_RATING_LOCAL:
            return {
                ...state,
                isAdded: true
            }
        case RATINGFORM_IS_RATING_ADDED_PENDING:
            return {
                ...state,
                isAddedPending:true
            }
        case RATINGFORM_ISRATINGADDED:
            return{
                ...state,
                isAdded:action.payload.data.success
            }
        case RATINGFORM_CLEAR:
            return INITIAL_STATE;
        default:
            return state;
    }
};
