/**
 * Created by radims on 05.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");
/**
 * Class represents structure of line of file dlp_cesty.csv document
 */
class Dlp_cesty_entity {

    constructor(type, name, name_en, name_lat){
        if (jsutils.isSomeEmpty([type,name,name_en,name_lat])) {
            throw new Error("class dlp_cesty_entity: all variables in costructor are mandatory")
        }
        this.type = type;
        this.name = name;
        this.name_en = name_en;
        this.name_lat = name_lat;
    }


}

module.exports = Dlp_cesty_entity;
