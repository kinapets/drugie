/**
 * Created by radims on 05.03.16.
 */

var mapper = require("../sukl/model/dlp_slozeni/dlp_slozeni_mapper");
var owlrdf_presenter = require("./_base_rdfowl_presenter");
var SuklVersion = require("../../app/sukl/versions/versions");
var base_presenter = require("./_base_presenter");

var basicFileMapper = require("../sukl/model/basic-file-mapper");
var template_system = require("../../modules/template-system/template-system");

const TEMPLATE_PATH_DEFAULT = "dlp_slozeni/default.html";
const TEMPLATE_PATH_RDF = "dlp_slozeni/rdf.html";

/**
 * Generate owl file of dlp_doping
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters
 */
module.exports.defaultAction = function(params, config,callback) {
    callback = callback || console.log;
    owlrdf_presenter.defaultAction(
        params,
        config,
        TEMPLATE_PATH_DEFAULT,
        config.file_names.sukl.dlp_slozeni,
        mapper,
        ".owl",
        callback
    );
};


/**
 * Generate rdf file of dlp_doping
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters
 */
module.exports.rdfAction = function(params, config,callback) {
    callback = callback || console.log;
    params[2] = "dlp_slozeni";
    owlrdf_presenter.defaultAction(
        params,
        config,
        TEMPLATE_PATH_RDF,
        config.file_names.sukl.dlp_slozeni,
        mapper,
        ".rdf",
        callback
    );
};



