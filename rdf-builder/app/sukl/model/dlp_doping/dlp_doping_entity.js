/**
 * Created by radims on 06.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_doping_entity{
    constructor(doping, nazev){
        if (jsutils.isSomeEmpty([doping,nazev])) {
            throw new Error("class dlp_cesty_entity: all variables in costructor are mandatory")
        }
        this.doping = doping;
        this.nazev = nazev;
    }

}

module.exports = Dlp_doping_entity;