/**
 * Created by radims on 06.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_formy_entity{
    constructor(cesta, forma, nazev, nazev_en,nazev_lat){
        if (jsutils.isSomeEmpty([cesta, forma, nazev, nazev_en,nazev_lat])) {
            throw new Error("class dlp_cesty_entity: all variables in costructor are mandatory")
        }

        this.cesta = cesta;
        this.forma = forma.replace(new RegExp(" ", 'g'), "_");
        this.nazev = nazev;
        this.nazev_en = nazev_en;
        this.nazev_lat = nazev_lat;
    }

}

module.exports = Dlp_formy_entity;