import React from 'react';
import { Component } from 'react';




export default class InteractionPairImage extends Component {
    render() {
        const url = this.props.drugBankUrl;
        const drugBankID = url.split("/")[url.split("/").length - 1].split("#")[0];
        return (
            <div>
                <img src={`http://moldb.wishartlab.com/molecules/${drugBankID}/thumb.svg`}/>
            </div>
        );
    }
}

