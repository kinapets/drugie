/**
 * Created by radims on 27.03.16.
 */
import {
    LOGIN_LOGIN,
    LOGIN_SAVE_ACCESS_TOKEN,
    LOGIN_AUTHORIZE,
    LOGIN_LOAD_ACCESS_TOKEN,
    LOGIN_CLEAR_ACCESS_TOKEN,
    LOGIN_AUTHORIZE_PENDING
} from "../actions/login_actions";

const INITIAL_STATE = {
    loggedIn : null, // je uzivatel prihlasen
    status:null,
    accessToken : null,//access token
    userId: null, //id prihlaseneho usera
    user: null,  //objekt usera
    savedInCookie: false, //jestli bylo uloženo již cookie
    isAuthorizePending: false, //jestli prave autorizace probiha - byl odeslan request
};

export default function(state = INITIAL_STATE, action) {
    switch (action.type){
        case LOGIN_AUTHORIZE:
            var requestData = action.payload.data;
            return{
                ... state,
                loggedIn:requestData.status,
                status:requestData.status,
                userId: requestData.userId,
                user: requestData.user,
                savedInCookie: true
            }
        case LOGIN_AUTHORIZE_PENDING:
            return {
                ...state,
                isAuthorizePending: true
            }
        case LOGIN_LOGIN:
            var requestData = action.payload.data;
            return{
                ... state,
                loggedIn:requestData.status,
                status:requestData.status,
                accessToken: requestData.accessToken,
                userId: requestData.userId,
                user: requestData.user,

            }
        case LOGIN_SAVE_ACCESS_TOKEN:
            return {
                ...state,
                accessToken: action.payload,
                savedInCookie: true
            }
        case LOGIN_LOAD_ACCESS_TOKEN:
            return{
                ...state,
                accessToken: action.payload
            }
        case LOGIN_CLEAR_ACCESS_TOKEN:
            return INITIAL_STATE
        default:
            return state;
    }
};
