var express = require('express');
var router = express.Router();
var User = require("./../mongo_model/user_schema").Model;
var passwordHash = require('password-hash');


/**
 * REgistrace uživatelů
 */
router.post('/register', function(req, res) {
    const formValues = req.body;

    new User({
        username : formValues.email.value,
        firstName : formValues.firstName.value,
        lastName: formValues.lastName.value,
        password: passwordHash.generate(formValues.password.value),
    })
    .save(function(err, user){
        console.log(user);
        return err
            ? res.json({success: false})
            : res.json({success: true});
    });
});

/**
 * Kontrola zda není už email registrován
 */
router.post('/isregistered', function(req, res) {
    const username = req.body.email;
    User.findByUsername(username, function (err, user) {
        if (err) return res.json({result:false});
        return user
            ? res.json({result: true})
            : res.json({result: false});
    });
});

/**
 * Routa pro login
 */
router.post("/login", function (req, res) {
    const username = req.body.username;
    const password = req.body.password;
    const loginFail = {
        status: false,
        accessToken: null,
        userId: null,
        user: null
    };

    User.findByUsername(username, function (err,user) {
        //neexistuje uživatel nebo problem s db
        if(!user || err) {
            res.json(loginFail);
        }
        //verifikace hesla
        else if(passwordHash.verify(String(password), user.password)){
            //pokudj e uzivatel ok tak se mu vytvori novy token
            user.generateNewAccessToken(function(err,user) {
                if(err) res.json(loginFail);
                else if(user) {
                    res.json({
                        status: true,
                        accessToken:user.accessToken,
                        userId: user._id,
                        user: user
                    });
                }
            })
        }else {
            res.json(loginFail);
        }
    });
});


router.post("/authorize", function (req, res) {
    const accessToken = req.body.accessToken;

    User.findByAccessToken(accessToken, function (err,user) {
        res.json({
            status: true,
            userId: user._id,
            user: {
                firstName: user.firstName,
                lastName: user.lastName,
                username: user.username
            }
        });
    });
});


module.exports = router;
