
/**
 * Created by radims on 12.02.16.
 */
var jsutils = require("../../../../modules/utils/jsutils");

class Dlp_obaly {
    constructor(obal, nazev, nazev_en){
        if (jsutils.isSomeEmpty([obal, nazev])) {
            throw new Error("class: all variables in costructor are mandatory")
        }
        this.obal = obal;
        this.nazev = nazev;
        this.nazev_en = nazev_en;
    }

}

module.exports = Dlp_obaly;