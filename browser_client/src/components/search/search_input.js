/**
 * Created by radims on 02.04.16.
 */
import React from 'react';
import { Component } from 'react';


export default class SearchInput extends Component {
    render() {
        return (
            <div className="search-block-v2">
                <div className="container">
                    <div className="col-md-6 col-md-offset-3">
                        <h2>{this.props.title}</h2>
                        <div className="input-group">
                            <input
                                type="text"
                                className="form-control"
                                placeholder={this.props.placeholder}
                                value={this.props.inputValue}
                                onChange={this.props.onInputChange}
                            />
                    <span className="input-group-btn">
                        <button
                            onClick={this.props.onButtonClick}
                            className="btn-u"
                            type="button">
                            <i className="fa fa-search"></i>
                        </button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}



