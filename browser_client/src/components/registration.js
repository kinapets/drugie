/**
 * Created by radims on 07.04.16.
 */
/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import Menu from "./common/menu";
import Breadcrumb from "./common/breadcrumb";
import {connect} from "react-redux";
import translations from "../services/translator/translator";
import RegistrationForm from "./registration/form";

class Registration extends Component {
    render() {
        const ACTIVE_LANG = this.props.langs.active;
        return (
            <div>
                <Menu/>
                <Breadcrumb
                    title={`${translations.registration.breadcrumb_title[ACTIVE_LANG]}`}
                    pages={[{linkTo:"/", title: "Drugie"}]}
                    last={translations.registration.breadcrumb_page[ACTIVE_LANG]}
                />
                <div className="container content">
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                            <RegistrationForm/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        langs: state.langs
    };
}

export default connect(mapStateToProps, {null})(Registration);