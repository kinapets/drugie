/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import translations from "../../services/translator/translator";
import {Panel} from "react-bootstrap";
import {connect} from "react-redux";

class DrugTable extends Component {
    render() {

        const ACTIVE_LANG = this.props.langs.active;
        return (
            <Panel collapsible defaultExpanded header={`${translations.drug_detail.basic_info[ACTIVE_LANG]}`}>
                <table className="table">
                    <tbody>
                    <tr>
                        <th>{translations.drug_detail.kod_sukl[ACTIVE_LANG]}</th>
                        <td>{this.props.kod_sukl}</td>
                    </tr>
                    <tr>
                        <th>{translations.drug_detail.sila[ACTIVE_LANG]}</th>
                        <td>{this.props.sila}</td>
                    </tr>
                    <tr>
                        <th>{translations.drug_detail.atc[ACTIVE_LANG]}</th>
                        <td>{this.props.atc_name}</td>
                    </tr>
                    <tr>
                        <th>{translations.drug_detail.atc_name_en[ACTIVE_LANG]}</th>
                        <td>{this.props.atc_name_en}</td>
                    </tr>
                    <tr>
                        <th>{translations.drug_detail.atc_who[ACTIVE_LANG]}</th>
                        <td>{this.props.atc_who}</td>
                    </tr>
                    <tr>
                        <th>{translations.drug_detail.brailovo_pismo[ACTIVE_LANG]}</th>
                        <td>{this.getBrailovoPismoText(this.props.brailovo_pismo)}</td>
                    </tr>


                    </tbody>
                </table>
            </Panel>
        );
    }

    getBrailovoPismoText(text){
        const ACTIVE_LANG = this.props.langs.active;
        if(text === "S")
            return translations.drug_detail.s[ACTIVE_LANG];
        else
            return translations.drug_detail.v[ACTIVE_LANG];
    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        dlp_lecivepripravky: state.dlp_lecivepripravky,
        langs: state.langs
    };
}

export default connect(mapStateToProps, null)(DrugTable);