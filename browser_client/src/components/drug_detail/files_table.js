/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import translations from "../../services/translator/translator";
import {Panel} from "react-bootstrap";

import {connect} from "react-redux";

class FilesTable extends Component {



    render() {
        const SPC = "spc";
        const PIL = "pil";
        const OBAL_TEXT = "obal";
        const ACTIVE_LANG = this.props.langs.active;

        if(!this.props.drug.spc)
            return (
                <Panel collapsible header={translations.files_table.table_title[ACTIVE_LANG]}>
                    {translations.files_table.no_files[ACTIVE_LANG]}
                </Panel>
            );

        return (
            <Panel collapsible header={translations.files_table.table_title[ACTIVE_LANG]}>
            <table className="table">
                <tbody>
                    <tr>
                        <th>{translations.files_table.pil[ACTIVE_LANG]}</th>
                        <td>
                            <a href={this.generateUrl(this.props.drug.pil,PIL, this.props.drug.nazev.value)}>
                                <i className="fa fa-file-pdf-o"></i>
                                {this.props.drug.pil.value}
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th>{translations.files_table.spc[ACTIVE_LANG]}</th>
                        <td>
                            <a href={this.generateUrl(this.props.drug.spc,SPC, this.props.drug.nazev.value)}>
                                <i className="fa fa-file-pdf-o"></i>
                                {this.props.drug.spc.value}
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th>{translations.files_table.obal_text[ACTIVE_LANG]}</th>
                        <td>
                            <a href={this.generateUrl(this.props.drug.obal_text,OBAL_TEXT, this.props.drug.nazev.value)}>
                                <i className="fa fa-file-pdf-o"></i>
                                {this.props.drug.obal_text.value}
                            </a>
                        </td>
                    </tr>

                </tbody>
            </table>
                </Panel>
        );
    }

    generateUrl(docName,docType,docTitle){
        return `http://www.sukl.cz/modules/medication/download.php?file=${docName.value}&type=${docType}&as=${docName.value}.pdf`
    }
}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        dlp_lecivepripravky: state.dlp_lecivepripravky,
        langs: state.langs
    };
}

export default connect(mapStateToProps, null)(FilesTable);/**
 * Created by radims on 03.04.16.
 */
