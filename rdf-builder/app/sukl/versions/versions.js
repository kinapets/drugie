/**
 * Created by radims on 23.01.16.
 */
var fs = require("fs");


class SuklVersions{

    constructor(data_sukl_base_path){
        this.data_sukl_base_dir = data_sukl_base_path;
        this.version = null;

    }

    getSuklPath(){
        return this.data_sukl_base_dir;
    }

    getLatestVersion(){
        if(this.version === null) {
            this.getAllVersion();
        }
        if(this.version.length === 0) {
            return null;
        }
        return this.version[0]
    };

    getVersion(date_int){
        date_int = Number(date_int);
        if(this.version === null) {
            this.getAllVersion();
        }
        if(this.version.length === 0) {
            return null;
        }
        var filter = this.version.filter((item)=>{
            return item.date_int === date_int;
        });

        return filter.length === 0 ? null : filter[0];

    }

    getAllVersion(){
        var dir = fs.readdirSync(this.data_sukl_base_dir).filter((item)=>{
            return fs.lstatSync(this.data_sukl_base_dir + item).isDirectory();
        });

        var dirObj = dir.map((item)=> {
            return {
                name: item,
                date_int: Number(item.replace("DLP","")),
                directory: this.data_sukl_base_dir+item+"/"
            };
        });

        return this.version =(dirObj.sort((a,b)=> {
            if(a.date_int   < b.date_int) return 1;
            if(a.date_int > b.date_int) return -1;
            return 0;
        }));
    }
}

module.exports = SuklVersions;

