/**
 * Created by radims on 29.01.16.
 */

var fs = require("fs");
var pth = require("path");
var concat_files = require("concat-files");
var async = require("async");

var atc = require("./atc_presenter");
var dlp_doping = require("./dlp_doping_presenter");
var dlp_cesty = require("./dlp_cesty_presenter");
var dlp_formy = require("./dlp_formy_presenter");



/**
 *
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters

 */
module.exports.defaultAction = function (params,config) {
    console.log("build started");
    // 1st para in async.each() is the array of items
    var items = [
        atc.defaultAction,
        atc.rdfAction,
        dlp_cesty.defaultAction,
        dlp_cesty.rdfAction


    ];
    var presenter_path = config.base_paths.app.app_folder.presenters.base;
    var items = [];
    fs.readdirSync(presenter_path)
        .filter(function(item){
            return (
            pth.extname(item) !== ".js" ||
            item === "build_presenter.js" ||
            item.startsWith("_") ||
            item === "app.js"
            ) ? false : true;
        })
        .map(function(item){
            items.push(require("./" + item).defaultAction);
            items.push(require("./" + item).rdfAction);
        });




    // 1st para in async.each() is the array of items
    async.each(items,
        // 2nd param is the function that each item is passed to
        function(item, callback){
            // Call an asynchronous function, often a save() to DB
            item(params, config, callback);
        },
        // 3rd param is the function to call when everything's done
        function(err){
            // All tasks are done now
            console.log("done");
        }
    );






};

var wait = {
    defaultAction: function (params, config, callback) {
        setTimeout(function () {
            console.log("hello there")
            callback();
        }, 4000)
    },
    rdfAction: function(params,config,callback) {

    }
};




/**
 *
 * @param params - parameters of this action,
 *      params[1] - is number of version of sukl open data database
 * @param config - global app parameters

 */

function concatFiles(params,config) {
    var path = config.base_paths.app.temp.data.sukl;
    fs.readdir(path,function(err,files) {
        if(err) {
            throw err;
        }
        var filter = files
            .map(function(item){
                return path + item
            })
            .filter(function(item) {
                return pth.extname(item) === ".owl" || pth.extname(item) === ".rdf";
            })
        concat_files(filter, path + "db",function() {
            console.log("Concating was done");
        });


    });

};


module.exports.concatAction = concatFiles;