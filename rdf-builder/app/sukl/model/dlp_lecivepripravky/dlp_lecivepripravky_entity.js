/**
 * Created by radims on 19.02.16.
 */

class Dlp_lecivepripravky {

    constructor(kod_sukl,h,nazev,doplnek,cesta,forma,baleni,sila,obal, drz,zemdr,akt_drz,akt_zem,reg,v_platdo,neomez,
                uvadenido,is_,atc_who,rc,sdov,sdov_dov,sdov_zem,reg_proc,dddamnt_who,dddun_who,ddd,zdroj_who, ll, vydej,
                zav, doping,narvla, dodavky, ean, braillovo_pismo, exp, exp_t)
    {
        //Kód léčivého přípravku (dále LP) přidělený SÚKL variantě LP v rámci rozhodnutí o registraci LP, přidělený
        // neregistrovanému LP zařazenému do specifického léčebného programu (dále SLP) nebo přidělený potravině pro
        // zvláštní lékařské účely (dále PZLÚ)
        this.kod_sukl = kod_sukl;
        //Označení LP, který podléhá povinnému hlášení (povinnost předkládat Ústavu před propuštěním na trh vzorky
        // každé šarže přípravku k přezkoušení, § 32 odst. 4 písm.a) zákona o léčivech
        this.h= h;
        //Název LP nebo PZLÚ
        this.nazev = nazev;
        //Doplněk názvu LP, který jednoznačně určuje variantu LP, sestávající z integrace cesty podání LP, jeho lékové
        //formy, velikosti balení a síly. Jedná se o starší položku číselníku, dále upřesněnou v položkách CESTA, FORMA,
        // BALENÍ a SÍLA
        this.doplnek = doplnek;
        //Cesta podání
        this.cesta = cesta;
        //Léková forma
        this.forma = forma.replace(new RegExp(" ", 'g'), "_");
        //Velikost balení
        this.baleni = baleni;
        //Síla LP, kterou se rozumí obsah léčivých látek vyjádřený kvantitativně vzhledem k jednotce dávky, objemu nebo
        // hmotnosti podle lékové formy
        this.sila = sila;
        //Vnitřní obal LP, kterým se rozumí taková forma obalu, který je v bezprostředním kontaktu s LP
        this.obal = obal;
        //Držitel rozhodnutí o registraci
        this.drz = drz;
        //Země sídla držitele rozhodnutí o registraci
        this.zemdr = zemdr;
        //Aktuální držitel reg.rozhodnutí. Uvádí se jen u registrací B
        this.akt_drz = akt_drz;
        //Země aktuálního držitele rozhodnutí o reg.
        this.akt_zem = akt_zem;
        //Stav registrace
        this.reg = reg;
        //Platnost registrace do, není-li stanovena neomezená platnost podle § 34 zákona o léčivech. Uvádí se jen u
        // registrací B, C, Y, F
        this.v_platdo = v_platdo;
        //V poli vyplněno (X) v případě neomezené  platnosti registrace (podle § 34 zákona o léčivech)
        this.neomez = neomez;
        //Uvádění na trh do, není-li stanovena neomezená platnost podle § 34 zákona o léčivech. Uvádí se jen u
        // registrací B, C, Y
        this.uvadenido = uvadenido;
        //Indikační skupina
        this.is_ = is_;
        //Anatomicko-terapeuticko-chemická skupina (WHO Index)
        this.atc_who = atc_who;
        //Registrační číslo
        this.rc = rc;
        //Identifikační číslo souběžného dovozu, které se váže na odkazovaný referenční přípravek dle registračního
        // čísla; zpravidla ve tvaru PI/xxx/yyyy"
        this.sdov = sdov
        //Zkratka dovozce u souběžného dovozu
        this.sdov_dov = sdov_dov;
        //Země dovozce u souběžného dovozu
        this.sdov_zem = sdov_zem;
        //Registrační procedura
        this.reg_proc = reg_proc;
        //množství léčivé látky v definované denní dávce (DDD)
        this.dddamnt_who = dddamnt_who;
        //jednotka množství léčivé látky v DDD
        this.dddun_who = dddun_who;
        //Počet DDD v balení (na 4 desetinná místa)
        this.ddd = ddd;
        //WHO Index
        this.zdroj_who = zdroj_who;
        //Seznam léčivých látek, oddělený čárkami

        this.ll = ll.split(",");
        this.ll.filter(function(item) {
            return (this.isNormalInteger(item.trim()))
        }.bind(this))
        //Klasifikace typu výdeje léčivého přípravku
        this.vydej = vydej;
        //Závislost - skupina omamných nebo psychotropních látek (návykové látky)
        this.zav = zav;
        //Doping
        this.doping = doping;
        //Označení látky s anabolickým a jiným hormonálním účinkem podle nařízení vlády 454/2009 Sb.-
        this.narvla = narvla;
        //Přípravek byl dodáván do lékáren a zdr. zařízení v posledním 3 měsících
        this.dodavky = dodavky;
        //Seznam EAN kódů, oddělený čárkami
        this.ean = ean;
        //Braillovo písmo: schváleno (S), výjimka (V), nežádáno (mezera)
        this.braillovo_pismo = braillovo_pismo;
        //Expirace
        this.exp = exp;
        //Expirace typ: hodiny H, dny D, týdny W, měsíce M
        this.expt_t = exp_t;




    }

    isNormalInteger(str) {
        var n = ~~Number(str);
        return String(n) === str && n >= 0;
    }


}

module.exports = Dlp_lecivepripravky;