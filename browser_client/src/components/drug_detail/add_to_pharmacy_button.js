/**
 * Created by radims on 27.03.16.
 */
import React from 'react';
import { Component } from 'react';
import {connect} from "react-redux";
import Loader from "../common/loader";

import {addToMyPharmacy,removeFromMyPharmacy,fetchMyPharmacy} from "../../actions/mypharmacy_actions";

class AddToPharmacyButton extends Component {
    /**
     * stažení dat ze serveru
     */
    componentDidUpdate(){
        const access_token = this.props.loginReducer.accessToken;
        const isFetched = this.props.myPharmacy.fetched;

        if(access_token && !isFetched)
            this.props.fetchMyPharmacy(access_token);
    }

    onAddToPharmacyClick(event) {
        const drug = this.props.dlp_lecivepripravky.detail_drug[0];
        const access_token = this.props.loginReducer.accessToken;

        this.props.addToMyPharmacy(drug, access_token);
    }
    onRemoveToPharmacyClick(event) {
        const drug = this.props.dlp_lecivepripravky.detail_drug[0];
        const access_token = this.props.loginReducer.accessToken;

        this.props.removeFromMyPharmacy(drug, access_token);
    }

    isInMyPharmacy(){
        if(!this.props.dlp_lecivepripravky.detail_drug){
            return false;//drug detail není jeste nacten
        }
        const drug_list = this.props.myPharmacy.drug_list;
        const drug = this.props.dlp_lecivepripravky.detail_drug[0];
        var filter_list = drug_list.filter(function (item) {
            return drug.kod_sukl.value == item.kod_sukl.value
        });
        return Boolean(filter_list.length);
    }

    render() {
        const isFetched = this.props.myPharmacy.fetched;

        if (!isFetched)
            return <button className="pull-right" ><Loader widthLoader={30}/></button>;

        if(!this.isInMyPharmacy())
            return (
                <button
                    onClick={this.onAddToPharmacyClick.bind(this)}
                    className="pull-right">
                    <i className="fa fa-medkit"></i>
                </button>
            );
        else
            return (
                <button
                    onClick={this.onRemoveToPharmacyClick.bind(this)}
                    className="pull-right btn btn-success">
                    <i className="fa fa-check"></i>
                </button>
            );
    }
}


const actions = {addToMyPharmacy, fetchMyPharmacy,removeFromMyPharmacy}

//namapování stavu do props
function mapStateToProps(state) {
    return {
        dlp_lecivepripravky: state.dlp_lecivepripravky,
        langs: state.langs,
        loginReducer: state.login,
        myPharmacy: state.myPharmacy
    };
}

export default connect(mapStateToProps, actions)(AddToPharmacyButton);
