/**
 * Created by radims on 07.04.16.
 */

import React from 'react';
import { Component } from 'react';

export default class InputFormGroup extends Component {
    render() {
        const field = this.props.field;
        const type = this.props.type || "text";

        return (
            <div className="input-group margin-bottom-20">
                <span className="input-group-addon"><i className={`fa fa-${this.props.icon}`}></i></span>
                <input type={type} placeholder={this.props.placeholder} className="form-control" {...field}/>
                <div className="control-label" >
                    {
                        field.touched ? field.error : ""
                    }
                </div>
            </div>
        );
    }
}

