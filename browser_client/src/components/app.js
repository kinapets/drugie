import React from 'react';
import { Component } from 'react';
import Menu from "./common/menu";

export default class App extends Component {
    render() {
        return (
            <div>
                {this.props.children}
            </div>
        );
    }
}
