/**
 * Created by radims on 12.03.16.
 */
var query_loader = require("../query_loader");


module.exports = function(container) {return {

    /**
     * Returns all drugs int atc group
     * @param atc
     */
    findAllByAtc: function(atc) {
        container.conn.query({
                database: "drug_allergy",
                query:
                "PREFIX dlp_lecivepripravky: <http://db.cz/owl/dlp_lecivepripravky/> " +
                "select distinct ?s ?p ?o where { ?s dlp_lecivepripravky:atc_who '"+atc+"'.}",
                limit: 10,
                offset: 0
            },
            function (data) {
                console.log(data);
                console.log(data.results.bindings);

            });
    },
    /**
     * Returns all drugs int atc group
     * @param atc
     */
    findAllByName: function(name,limit, offset,callback) {
        limit = limit || 10;
        offset = offset || 0;
        var str = query_loader.load(query_loader.FIND_ALL_BY_NAME_PATTERN, {name: name})
        container.conn.query({
                database: "drug_allergy",
                query:
                str,
                limit: limit,
                offset: offset
            },
            function (data) {
                callback(data.results.bindings);
            });
    },    /**
     * Returns all drugs int atc group
     * @param atc
     */
    countByName: function(name,callback) {
        var str = query_loader.load(query_loader.COUNT_BY_NAME_PATTERN, {name: name})
        container.conn.query({
                database: "drug_allergy",
                query:
                str,
                limit: 100000,
                offset: 0
            },
            function (data) {
                console.log(data)
                callback(data.results.bindings);
            });
    },
    countAllSimilarDrugByATC: function(id,callback) {
        var str = query_loader.load(query_loader.COUNT_ALL_SIMILAR_DRUG_BY_ATC, {id: id})
        container.conn.query({
                database: "drug_allergy",
                query:
                str,
                limit: 100000,
                offset: 0
            },
            function (data) {
                console.log(data)
                callback(data.results.bindings);
            });
    },
    /**
     * Returns all drugs int atc group
     * @param atc
     */
    findById: function(id,callback) {
        var str = query_loader.load(query_loader.FIND_BY_ID, {id: id})
        container.conn.query({
                database: "drug_allergy",
                query:
                str,
                limit: 1,
                offset: 0
            },
            function (data) {
                callback(data.results.bindings);
            });
    },
    /**
     * Returns all drugs int atc group
     * @param atc
     */
    findDrugFormulationByDrugId: function(id,callback) {
        var str = query_loader.load(query_loader.FIND_DRUG_FORMULATION_BY_DRUG_ID, {id: id})
        container.conn.query({
                database: "drug_allergy",
                query:
                str,
                limit: 100,
                offset: 0
            },
            function (data) {
                callback(data.results.bindings);
            });
    },
    /**
     * vrací všechny léky podobné jinému - dle jeho kod sukl
     * @param id
     * @param callback
     */
    findAllSimilarDrugByATC: function(id,limit, offset,callback){
        limit = limit || 10;
        offset = offset || 0;
        var str = query_loader.load(query_loader.FIND_ALL_SIMILAR_DRUG_BY_ATC, {id: id})
        container.conn.query({
                database: "drug_allergy",
                query:
                str,
                limit: limit,
                offset: offset
            },
            function (data) {
                callback(data.results.bindings);
            });
    }

};};
